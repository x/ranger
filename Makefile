# Copyright 2018 AT&T Intellectual Property.  All other rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

DOCKER_REGISTRY            ?= quay.io
IMAGE_NAME                 := ranger rangercli
IMAGE_PREFIX               ?= attcomdev
IMAGE_TAG                  ?= latest
HELM                       ?= helm
LABEL                      ?= miscellaneous
COMMIT                     := $(shell git rev-parse HEAD)
PROXY                      ?= http://foo.proxy.com:8000
PUSH_IMAGE                 ?= false
LATEST                     := latest
NO_PROXY                   ?= localhost,127.0.0.1,.svc.cluster.local
USE_PROXY                  ?= false
RANGER_USER                ?= ranger
BASE_IMAGE                 ?= ubuntu:18.04

IMAGE_DIR:=images/$(IMAGE_NAME)


#make images will build ranger and rangercli
.PHONY: images
images: $(IMAGE_NAME)
 $(IMAGE_NAME):
	@echo "  "
	@echo "===== Processing [$@] image ====="
	@make build_$@ IMAGE=${DOCKER_REGISTRY}/${IMAGE_PREFIX}/$@:${IMAGE_TAG} BASE_IMAGE=${BASE_IMAGE} IMAGE_DIR=images/$@ IMAGE_NAME=$@

# Create tgz of the chart
.PHONY: charts
charts: clean
	$(HELM) dep up charts/ranger
	$(HELM) package charts/ranger

# Perform linting
.PHONY: lint
lint: pep8 helm_lint

# Dry run templating of chart
.PHONY: dry-run
dry-run: clean
	tools/helm_tk.sh $(HELM)

# Make targets intended for use by the primary targets above.
.PHONY: build_ranger
build_ranger:

ifeq ($(USE_PROXY), true)
	docker build --network host -t $(IMAGE) --label $(LABEL) -f $(IMAGE_DIR)/Dockerfile \
		--build-arg user=$(RANGER_USER) \
		--build-arg BASE_IMAGE=${BASE_IMAGE} \
		--build-arg http_proxy=$(PROXY) \
		--build-arg https_proxy=$(PROXY) \
		--build-arg HTTP_PROXY=$(PROXY) \
		--build-arg HTTPS_PROXY=$(PROXY) \
		--build-arg no_proxy=$(NO_PROXY) \
		--build-arg NO_PROXY=$(NO_PROXY) .
else
	docker build --network host -t $(IMAGE) --label $(LABEL) -f $(IMAGE_DIR)/Dockerfile --build-arg BASE_IMAGE=${BASE_IMAGE} --build-arg user=$(RANGER_USER) .
endif

ifeq ($(PUSH_IMAGE), true)
	docker push $(IMAGE)
	docker tag $(IMAGE) ${DOCKER_REGISTRY}/${IMAGE_PREFIX}/ranger:$(COMMIT)
	docker push ${DOCKER_REGISTRY}/${IMAGE_PREFIX}/ranger:$(COMMIT)
endif

.PHONY: build_rangercli
build_rangercli:

ifeq ($(USE_PROXY), true)
	docker build --network host -t $(IMAGE) --label $(LABEL) -f $(IMAGE_DIR)/Dockerfile \
		--build-arg BASE_IMAGE=${BASE_IMAGE} \
		--build-arg http_proxy=$(PROXY) \
		--build-arg https_proxy=$(PROXY) \
		--build-arg HTTP_PROXY=$(PROXY) \
		--build-arg HTTPS_PROXY=$(PROXY) \
		--build-arg no_proxy=$(NO_PROXY) \
		--build-arg NO_PROXY=$(NO_PROXY) .
else
	docker build --network host -t $(IMAGE) --label $(LABEL) --build-arg BASE_IMAGE=${BASE_IMAGE} -f $(IMAGE_DIR)/Dockerfile .
endif

.PHONY: clean
clean:
	rm -rf build
	helm delete helm-template ||:

.PHONY: pep8
pep8:
	cd ../../; tox -e pep8

.PHONY: helm_lint
helm_lint: clean
	tools/helm_tk.sh $(HELM)
	$(HELM) lint charts/ranger
