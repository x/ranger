import logging
from orm.common.orm_common.model.models import ResourceStatusModel, StatusModel
from orm.common.orm_common.sql_alchemy.db_models import ResourceStatus
import time

from oslo_config import cfg

logger = logging.getLogger(__name__)


class ResourceStatusRecord:
    def __init__(self, session):

        self.__resource_status = ResourceStatus()
        self.__TableName = "resource_status"

        if session:
            self.setDBSession(session)

    def setDBSession(self, session):
        self.session = session

    @property
    def resource_status(self):
        return self.__resource_status

    @resource_status.setter
    def resource_status(self, resource_status):
        self.__resource_status = resource_status

    def read_resource_status(self, resource_uuids, extended):
        records_model = {}
        statuses_model = {}

        timestamp = int(time.time()) * 1000

        if extended:
            max_interval_in_seconds = \
                cfg.CONF.resource_status_extended_wait_time
        else:
            max_interval_in_seconds = cfg.CONF.resource_status_wait_time

        ref_timestamp = (int(time.time()) - max_interval_in_seconds) * 1000

        try:
            records = self.session.query(
                ResourceStatus.id,
                ResourceStatus.timestamp,
                ResourceStatus.region,
                ResourceStatus.status,
                ResourceStatus.transaction_id,
                ResourceStatus.resource_id,
                ResourceStatus.ord_notifier,
                ResourceStatus.err_msg,
                ResourceStatus.err_code,
                ResourceStatus.operation).filter(
                    ResourceStatus.resource_id.in_(resource_uuids)).all()

            if records:
                for id, timestamp, region, status, transaction, resource, \
                        notifier, err_msg, err_code, operation in records:
                    if (status == "Submitted"
                            and timestamp < ref_timestamp):
                        timestamp = timestamp
                        status = "Error"
                        err_msg = "Status updated to 'Error'. " \
                            "Too long 'Submitted' status"

                    status_model = ResourceStatusModel(
                        timestamp,
                        region,
                        status,
                        transaction,
                        resource,
                        notifier,
                        err_msg,
                        err_code,
                        operation)

                    if resource in records_model:
                        records_model[resource].append(status_model)
                    else:
                        records_model[resource] = [status_model]

                for id, model in records_model.items():
                    statuses_model[id] = StatusModel(model)

            else:
                logger.debug("No resource status records found")

            return statuses_model
        except Exception as ex:
            message = "Failed to read resource status for id {}: {}".format(
                resource_id, str(ex))
            logger.exception(message)
            raise
