import logging
from orm.common.orm_common.sql_alchemy.db_models import RegionEndPoint
from sqlalchemy import and_

logger = logging.getLogger(__name__)


class RegionEndPointRecord:
    def __init__(self, session):

        self.__region_end_point = RegionEndPoint()
        self.__TableName = "region_end_point"

        if session:
            self.setDBSession(session)

    def setDBSession(self, session):
        self.session = session

    @property
    def region_end_point(self):
        return self.__region_end_point

    @region_end_point.setter
    def region_end_point(self, region_end_point):
        self.__region_end_point = region_end_point

    def read_region_end_point(self, region_id, end_point_type):
        try:
            record = self.session.query(RegionEndPoint).filter(
                and_(RegionEndPoint.region_id == region_id,
                     RegionEndPoint.end_point_type == end_point_type))
            sql_endpoint = record.first()

            if sql_endpoint:
                return sql_endpoint.public_url
            else:
                logger.debug("Public Url end point not found")
                return None

        except Exception as ex:
            message = "Failed to get Endpoint for region:{} type:{}-{}".format(
                region_id, endpoint_type, str(ex))
            logger.exception(message)
            raise
