from oslo_db.sqlalchemy import models
from sqlalchemy import BigInteger, Column, Integer, Text, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class CommonBaseModel(models.ModelBase):
    __table_args__ = {'mysql_engine': 'InnoDB'}


class ResourceStatus(Base, CommonBaseModel):
    __tablename__ = 'resource_status'

    id = Column(Integer, autoincrement=True, primary_key=True)
    timestamp = Column(BigInteger, primary_key=False)
    region = Column(Text, primary_key=False)
    status = Column(Text, primary_key=False)
    transaction_id = Column(Text, primary_key=False)
    resource_id = Column(Text, primary_key=False)
    ord_notifier = Column(Text, primary_key=False)
    err_code = Column(Text, primary_key=False)
    err_msg = Column(Text, primary_key=False)
    operation = Column(Text, primary_key=False)

    def __json__(self):
        return dict(
            id=self.id,
            timestamp=self.timestamp,
            region=self.region,
            status=self.status,
            transaction_id=self.transaction_id,
            resource_id=self.resource_id,
            ord_notifier=self.ord_notifier,
            err_code=self.err_code,
            err_msg=self.err_msg,
            operation=self.operation
        )


class RegionEndPoint(Base, CommonBaseModel):
    __tablename__ = 'region_end_point'

    region_id = Column(ForeignKey('region.region_id'), primary_key=True)
    end_point_type = Column(String(64), primary_key=True)
    public_url = Column(String(64), nullable=False)

    def __json__(self):
        return dict(
            end_point_type=self.end_point_type,
            public_url=self.public_url
        )
