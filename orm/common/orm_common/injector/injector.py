import importlib
import os
from oslo_log import log


from orm.common.orm_common.injector.fang import di
from orm.common.orm_common.utils.sanitize import sanitize_symbol_name

_di = di.Di()
LOG = log.getLogger(__name__)


def register_providers(env_variable, providers_dir_path, _logger):
    LOG.info('Initializing dependency injector')
    LOG.info('Checking {0} variable'.format(env_variable))

    env = None
    if not (env_variable in os.environ):
        LOG.info('No {0} variable found using prod injector'.format(env_variable))
        env = 'prod'
    elif os.environ[env_variable] == '__TEST__':
        LOG.info('__TEST__ variable found, explicitly skipping provider registration!')
        return
    else:
        env = os.environ[env_variable]
        log_message = \
            '{0} found, setting injector to {1} environment'.format(sanitize_symbol_name(env_variable), env)
        log_message = log_message.replace('\n', '_').replace('\r', '_')
        LOG.info(log_message)

    LOG.info('Setting injector providers')

    module = _import_file_by_name(env, providers_dir_path)


def get_di():
    return _di


def override_injected_dependency(dep_tuple):
    _di.providers.register_instance(dep_tuple[0], dep_tuple[1], allow_override=True)


def _import_file_by_name(env, providers_dir_path):
    file_path = os.path.join(providers_dir_path, '{0}_providers.py'.format(env))
    spec = importlib.util.spec_from_file_location("{0}_providers".format(env), file_path)
    module = importlib.util.module_from_spec(spec)
    if module is not None:
        spec.loader.exec_module(module)
        for provider in module.providers:
            LOG.info('Setting provider {0} to {1}'.format(provider[0], provider[1]))
            _di.providers.register_instance(provider[0], provider[1])
    else:
        LOG.error('Provider for {0} environment not found, path: {1} does not exist'.format(env, file_path))
    return module
