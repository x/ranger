"""Exceptions."""
import wsme


class ClientSideError(wsme.exc.ClientSideError):
    def __init__(self, message=None, status_code=400):
        self.message = message
        self.status_code = status_code
        super().__init__(msg=message, status_code=status_code)


class InputValueError(wsme.exc.ClientSideError):
    def __init__(self, message="Bad input value", status_code=400):
        self.message = message
        self.status_code = status_code
        super().__init__(msg=message, status_code=status_code)


class NotFoundError(wsme.exc.ClientSideError):
    def __init__(self, message="Query not found", status_code=404):
        self.message = message
        self.status_code = status_code
        super().__init__(msg=message, status_code=status_code)


class LockedEntity(wsme.exc.ClientSideError):
    def __init__(self, message="Entity locked", status_code=409):
        self.message = message
        self.status_code = status_code
        super().__init__(msg=message, status_code=status_code)


class NotAllowedError(wsme.exc.ClientSideError):
    def __init__(self, message="Method not allowed", status_code=405):
        self.message = message
        self.status_code = status_code
        super().__init__(msg=message, status_code=status_code)


class NoContentError(wsme.exc.ClientSideError):
    def __init__(self, message="Main json body empty", status_code=400):
        self.message = message
        self.status_code = status_code
        super().__init__(msg=message, status_code=status_code)


class UnauthorizedError(wsme.exc.ClientSideError):
    def __init__(self, message="Not allowed to perform this operation", status_code=401):
        self.message = message
        self.status_code = status_code
        super().__init__(msg=message, status_code=status_code)


class ErrorStatus(Exception):
    def __init__(self, message=None, status_code=400):
        self.status_code = status_code
        self.message = message
        super().__init__(message)


class ConflictError(Exception):
    def __init__(self, message="Item already exists", status_code=409):
        self.status_code = status_code
        self.message = message
        super().__init__(message)


class SQLDBError(Exception):
    def __init__(self, message="DB Error", status_code=409):
        self.status_code = status_code
        self.message = message
        super().__init__(message)


class EntityNotFound(Exception):
    def __init__(self, message="Entity not found in DB", status_code=404):
        self.status_code = status_code
        self.message = message
        super().__init__(message)
