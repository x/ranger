""" Storage base backend
"""


class ResourceStatusBase(object):
    def __init__(self, url):
        pass

    def add_update_status_record(self,
                                 timestamp,
                                 region,
                                 status,
                                 transaction_id,
                                 resource_id,
                                 ord_notifier,
                                 err_msg,
                                 err_code):
        raise NotImplementedError(Exception("Please Implement this method"))

    def get_records_by_resource_id(self, resource_id):
        raise NotImplementedError(Exception("Please Implement this method"))

    def get_records_by_filter_args(self, **filter_args):
        raise NotImplementedError(Exception("Please Implement this method"))


class ResourceTemplateBase(object):
    def __init__(self, url):
        pass

    def add_update_template_record(self,
                                   resource_id,
                                   stack_name, region,
                                   stack_template):
        raise NotImplementedError(Exception("Please Implement this method"))

    def get_records_by_transaction_id(self, transaction_id):
        raise NotImplementedError(Exception("Please Implement this method"))

    def get_trans_by_filter_args(self, **filter_args):
        raise NotImplementedError(Exception("Please Implement this method"))
