import logging
import oslo_db
import time

from oslo_db.sqlalchemy.enginefacade import LegacyEngineFacade
from pecan import conf

from orm.common.orm_common.model.models import ResourceStatusModel, StatusModel

from orm.services.resource_distributor.rds.services.model.region_resource_id_status \
    import RegionEndPointData
from orm.services.resource_distributor.rds.storage \
    import region_resource_id_status
from sqlalchemy \
    import BigInteger, BLOB, Column, ForeignKey, Integer, String, Text
from sqlalchemy.ext.declarative.api import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.sql import and_

Base = declarative_base()
logger = logging.getLogger(__name__)


class ResourceStatusRecord(Base):
    __tablename__ = 'resource_status'

    id = Column(Integer, autoincrement=True, primary_key=True)
    timestamp = Column(BigInteger, primary_key=False)
    region = Column(Text, primary_key=False)
    status = Column(Text, primary_key=False)
    transaction_id = Column(Text, primary_key=False)
    resource_id = Column(Text, primary_key=False)
    ord_notifier = Column(Text, primary_key=False)
    err_code = Column(Text, primary_key=False)
    err_msg = Column(Text, primary_key=False)
    operation = Column(Text, primary_key=False)
    resource_extra_metadata = relationship(
        "ImageMetadData", cascade="all, delete, delete-orphan")


class ImageMetadData(Base):
    __tablename__ = 'image_metadata'

    image_meta_data_id = Column(ForeignKey('resource_status.id'),
                                primary_key=True)
    checksum = Column(Text, primary_key=False)
    virtual_size = Column(Text, primary_key=False)
    size = Column(Text, primary_key=False)


class RegionEndPoint(Base):
    __tablename__ = 'region_end_point'

    region_id = Column(ForeignKey('region.region_id'), primary_key=True)
    end_point_type = Column(String(64), primary_key=True)
    public_url = Column(String(64), nullable=False)

    def __json__(self):
        return dict(
            end_point_type=self.end_point_type,
            public_url=self.public_url
        )

    def to_wsme(self):
        region = self.region_id
        url = self.public_url
        atype = self.end_point_type
        return RegionEndPointData(region, url, atype)


class ResourceTemplateRecord(Base):
    __tablename__ = 'resource_template_data'

    id = Column(Integer, autoincrement=True, primary_key=True)
    resource_id = Column(Text,
                         primary_key=False)
    resource_name = Column(Text, primary_key=False)
    region = Column(Text, primary_key=False)
    template_version = Column(Integer, primary_key=False)
    template_data = Column(BLOB, primary_key=False)


class ResStatusConnection(region_resource_id_status.ResourceStatusBase):
    """ Implements mysql DB """

    def __init__(self, url):
        self._engine_facade = LegacyEngineFacade(url)

    def add_update_status_record(self,
                                 timestamp,
                                 region,
                                 status,
                                 transaction_id,
                                 resource_id,
                                 ord_notifier,
                                 err_msg,
                                 err_code,
                                 operation,
                                 resource_extra_metadata=None):
        logger.debug("Add/Update status record:\ntimestamp [{}]\nregion [{}]"
                     "\nstatus [{}]\ntransaction_id [{}]\nresource_id [{}]\n"
                     "ord_notifier [{}]\nerr_code [{}]\n"
                     "err_msg [{}] operation [{}] resource_extra_metadata"
                     " [{}]".format(timestamp,
                                    region,
                                    status,
                                    transaction_id,
                                    resource_id,
                                    ord_notifier,
                                    err_code,
                                    err_msg,
                                    operation,
                                    resource_extra_metadata))
        try:
            session = self._engine_facade.get_session()
            with session.begin():
                image_metadata = None
                record = session.query(ResourceStatusRecord).\
                    filter_by(resource_id=resource_id, region=region).first()
                if resource_extra_metadata:
                    image_metadata = ImageMetadData(
                        checksum=resource_extra_metadata['checksum'],
                        virtual_size=resource_extra_metadata['virtual_size'],
                        size=resource_extra_metadata['size'])

                if record is not None:
                    logger.debug("Update record")
                    record.timestamp = timestamp
                    record.region = region
                    record.status = status
                    record.resource_id = resource_id
                    record.ord_notifier = ord_notifier
                    record.err_msg = err_msg
                    record.err_code = err_code
                    record.operation = operation
                    if record.resource_extra_metadata and image_metadata:
                        record.resource_extra_metadata[0] = image_metadata
                    elif image_metadata:
                        record.resource_extra_metadata.append(image_metadata)
                    else:
                        # remove child if not given
                        session.query(ImageMetadData).filter_by(
                            image_meta_data_id=record.id).delete()
                    return record.transaction_id
                else:
                    logger.debug("Add record")
                    resource_status = ResourceStatusRecord(
                        timestamp=timestamp,
                        region=region,
                        status=status,
                        transaction_id=transaction_id,
                        resource_id=resource_id,
                        ord_notifier=ord_notifier,
                        err_msg=err_msg,
                        err_code=err_code,
                        operation=operation)
                    if resource_extra_metadata:
                        resource_status.resource_extra_metadata.append(
                            image_metadata)

                    session.add(resource_status)
                    return transaction_id

        except oslo_db.exception.DBDuplicateEntry as e:
            logger.warning("Duplicate entry: {}".format(str(e)))

    def delete_resource_status_entry(self, resource_id, region_name):
        # note that when a resource_status entry is deleted, its corresponding
        # entry in resource_template_data is deleted as well
        try:
            session = self._engine_facade.get_session()
            with session.begin():
                record = session.query(ResourceStatusRecord).\
                    filter_by(resource_id=resource_id,
                              region=region_name).delete()
                if record is None:
                    logger.exception(
                        'Resource status data not found with resource id {} '
                        'and region name {}'.format(resource_id, region_name))

        except Exception as exp:
            raise

    def get_records_by_resource_id(self, resource_id):
        return self.get_records_by_filter_args(resource_id=resource_id)

    def get_resource_region_data(self, resource_id, region):
        logger.debug("Get resource data by resource_id '{}' and "
                     "region '{}'".format(resource_id, region))
        try:
            session = self._engine_facade.get_session()
            with session.begin():
                record = session.query(ResourceStatusRecord).\
                    filter_by(resource_id=resource_id, region=region).first()
                if record is None:
                    logger.exception(
                        'No resource status record with resource id {} and'
                        'region {} found'.format(resource_id, region))
                return record.resource_id, record.region
        except Exception as exp:
            raise

    def get_records_by_filter_args(self, **filter_args):
        logger.debug("Get records filtered by [{}]".format(filter_args))
        (timestamp, ref_timestamp) = self.get_timestamp_pair()
        logger.debug("timestamp=%s, ref_timestamp=%s" % (timestamp, ref_timestamp))
        records_model = []
        session = self._engine_facade.get_session()
        with session.begin():
            records = session.query(ResourceStatusRecord).filter_by(**filter_args)
            # if found records return these records
            if records is not None:
                for record in records:
                    if record.status == "Submitted" and record.timestamp < ref_timestamp:
                        record.timestamp = timestamp
                        record.status = "Error"
                        record.err_msg = "Status updated to 'Error'. Too long 'Submitted' status"

                    status = ResourceStatusModel(
                        record.timestamp,
                        record.region,
                        record.status,
                        record.transaction_id,
                        record.resource_id,
                        record.ord_notifier,
                        record.err_msg,
                        record.err_code,
                        record.operation,
                        record.resource_extra_metadata)

                    records_model.append(status)
                return StatusModel(records_model)
            else:
                logger.debug("No records found")
                return None

    def get_records_by_resource_id_and_status(self,
                                              status,
                                              resource_id,
                                              resource_type):
        """ This method filters all the records where resource_id is the given
        resource_id and status is the given status.
        for the matching records check if a time period elapsed and if so,
        change the status to 'Error' and the timestamp to the given timestamp.
        """
        logger.debug("Get records filtered by resource_id={} "
                     "and status={}".format(resource_id,
                                            status))

        (timestamp, ref_timestamp) = self.get_timestamp_pair(resource_type)
        logger.debug("timestamp=%s, ref_timestamp=%s" % (timestamp, ref_timestamp))
        session = self._engine_facade.get_session()
        records_model = []
        with session.begin():
            records = session.query(ResourceStatusRecord).\
                filter_by(resource_id=resource_id,
                          status=status)
            if records is not None:
                for record in records:
                    if record.status == "Submitted" and record.timestamp < ref_timestamp:
                        record.timestamp = timestamp
                        record.status = "Error"
                        record.err_msg = "Status updated to 'Error'. Too long 'Submitted' status"
                    else:
                        status = ResourceStatusModel(record.timestamp,
                                                     record.region,
                                                     record.status,
                                                     record.transaction_id,
                                                     record.resource_id,
                                                     record.ord_notifier,
                                                     record.err_msg,
                                                     record.err_code,
                                                     record.operation,
                                                     record.resource_extra_metadata)
                        records_model.append(status)
                if len(records_model):
                    return StatusModel(records_model)
            else:
                logger.debug("No records found")
            return None

    def get_timestamp_pair(self, resource_type=None):
        if resource_type == "customer":
            interval = conf.region_resource_id_status.max_interval_time.tenants
        elif resource_type == "flavor":
            interval = conf.region_resource_id_status.max_interval_time.flavors
        elif resource_type == "image":
            interval = conf.region_resource_id_status.max_interval_time.images
        else:
            interval = conf.region_resource_id_status.max_interval_time.default

        timestamp = int(time.time())
        ref_timestamp = timestamp - interval
        return timestamp * 1000, ref_timestamp * 1000

    def get_region_keystone_ep(self, region_name):
        """get keystone url from region record """
        logger.debug("Get region keystone endpoint: {}".format(region_name))

        try:
            key_ep = 'identity'
            session = self._engine_facade.get_session()
            with session.begin():
                record = session.query(RegionEndPoint)
                record = record.filter(
                    and_(RegionEndPoint.region_id == region_name,
                         RegionEndPoint.end_point_type == key_ep))

                if record.first():
                    return record.first().public_url
                return None

        except Exception as exp:
            logger.exception("DB error RegionEndPoint filtered by region name")
            raise


class ResTemplateConnection(region_resource_id_status.ResourceTemplateBase):

    def __init__(self, url):
        self._engine_facade = LegacyEngineFacade(url)

    def add_update_template_record(self,
                                   resource_id,
                                   resource_name,
                                   region_name,
                                   resource_template):

        logger.debug("Add/Update template record:\nresource_id [{}]\n"
                     "region [{}]\n\n".format(resource_id, region_name))
        try:
            session = self._engine_facade.get_session()
            with session.begin():

                record = session.query(ResourceTemplateRecord).\
                    filter_by(resource_id=resource_id,
                              region=region_name).first()
                if record is not None:
                    logger.debug("Update resource template record")
                    record.resource_name = resource_name
                    record.template_version = record.template_version + 1
                    record.template_data = resource_template
                else:
                    logger.debug("Add resource template record")
                    resource_template_record = \
                        ResourceTemplateRecord(
                            resource_id=resource_id,
                            resource_name=resource_name,
                            region=region_name,
                            template_version=0,
                            template_data=resource_template)

                    session.add(resource_template_record)

        except oslo_db.exception.DBDuplicateEntry as e:
            logger.warning("Duplicate entry: {}".format(str(e)))

    def get_resource_template_data(self, resource_id, region):
        logger.debug("Get resource template data by resource {} and "
                     "region '{}' ".format(resource_id, region))
        try:
            session = self._engine_facade.get_session()
            with session.begin():
                record = session.query(ResourceTemplateRecord).\
                    filter_by(resource_id=resource_id,
                              region=region).first()
                if record is None:
                    logger.exception(
                        'No template record found with resource id {} '
                        'and region {} '.format(resource_id, region))

                return record
        except Exception as exp:
            raise
