from orm.services.resource_distributor.rds.storage.mysql.region_resource_id_status \
    import ResStatusConnection

from orm.services.resource_distributor.rds.storage.mysql.region_resource_id_status \
    import ResTemplateConnection


database = {
    'url': 'na'
}


def get_region_resource_id_status_connection():
    return ResStatusConnection(database['url'])


def get_resource_stack_data_connection():
    return ResTemplateConnection(database['url'])
