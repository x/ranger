import logging
import sys

from orm.common.orm_common.utils.error_base import InputValueError
from orm.services.resource_distributor.rds.storage import factory

logger = logging.getLogger(__name__)
config = {
    'max_interval_time': {
    },
    'allowed_status_values': {
    }
}

num_of_seconds_in_minute = 60
num_of_miliseconds_in_seconds = 1000


def add_update_template_data(data):
    logger.debug(
        "add/update template stack name [{}] ".format(data['resource_name']))

    try:
        conn = factory.get_resource_stack_data_connection()
        conn.add_update_template_record(data['resource_id'],
                                        data['resource_name'],
                                        data['region'],
                                        data['stack_template'])

    except Exception:
        logger.exception("Unexpected error: {}".format(sys.exc_info()[0]))
        raise


def delete_resource_status_data(resource_id, region):
    # delete resource status entry for the given resource_id and region
    logger.debug("delete resource status data for resource %s and "
                 "region %s" % (resource_id, region))
    conn = factory.get_region_resource_id_status_connection()
    resource_id, region = conn.get_resource_region_data(resource_id, region)
    conn = factory.get_region_resource_id_status_connection()
    conn.delete_resource_status_entry(resource_id, region)


def get_template_data(resource_id, region):
    logger.debug("get template data for resource %s and "
                 "region %s" % (resource_id, region))

    try:
        conn = factory.get_resource_stack_data_connection()
        record = conn.get_resource_template_data(resource_id, region)
        return record

    except Exception:
        logger.exception("Unexpected error: {}".format(sys.exc_info()[0]))
        raise


def add_status(data):
    logger.debug("add resource status timestamp [{}], region [{}], "
                 "status [{}], transaction_id [{}] and resource_id [{}], "
                 "ord_notifier_id [{}], error message [{}], error code [{}] "
                 "and resource_extra_metadata [{}]".format(
                     data['timestamp'],
                     data['region'],
                     data['status'],
                     data['transaction_id'],
                     data['resource_id'],
                     data['ord_notifier_id'],
                     data['error_msg'],
                     data['error_code'],
                     data.get('resource_extra_metadata', None)))

    try:
        validate_status_value(data['status'])
        validate_operation_type(data['resource_operation'])
        validate_resource_type(data['resource_type'])

        conn = factory.get_region_resource_id_status_connection()

        conn.add_update_status_record(
            data['timestamp'], data['region'], data['status'],
            data['transaction_id'], data['resource_id'],
            data['ord_notifier_id'], data['error_msg'],
            data['error_code'], data['resource_operation'],
            data.get('resource_extra_metadata'))

    except InputValueError:
        logger.exception("invalid inputs error")
        raise
    except Exception:
        logger.exception("Unexpected error: {}".format(sys.exc_info()[0]))
        raise


def get_status_by_resource_id(resource_id):
    logger.debug("get status by resource id %s " % resource_id)
    conn = factory.get_region_resource_id_status_connection()
    result = conn.get_records_by_resource_id(resource_id)
    return result


def get_regions_by_status_resource_id(status, resource_id, resource_type):
    logger.debug("get regions by status %s for %s resource %s" % (
        status, resource_type, resource_id))

    conn = factory.get_region_resource_id_status_connection()
    result = conn.get_records_by_resource_id_and_status(status,
                                                        resource_id,
                                                        resource_type)
    return result


def validate_resource_type(resource_type):
    allowed_resource_type = config['allowed_resource_type']
    if resource_type not in allowed_resource_type:
        logger.exception("status value is invalid: {}".format(resource_type))
        message = "{} not an allowed resource type".format(resource_type)
        raise InputValueError(message)


def validate_operation_type(operation_type):
    allowed_operation_type = config['allowed_operation_type']
    if operation_type not in allowed_operation_type:
        logger.exception("status value is invalid: {}".format(operation_type))
        message = "{} not an allowed operation type".format(operation_type)
        raise InputValueError(message)


def validate_status_value(status):
    allowed_status_values = config['allowed_status_values']
    if status not in allowed_status_values:
        logger.exception("status value is invalid: {}".format(status))
        message = "{} not an allowed status value".format(status)
        raise InputValueError(message)
