

class ResourceTemplateModel(object):
    def __init__(self,
                 resource_id,
                 resource_name,
                 region,
                 template_version,
                 template_data):
        self.resource_id = resource_id
        self.resource_name = resource_name
        self.region = region
        self.template_version = template_version
        self.template_data = template_data

    def as_dict(self):
        return self.__dict__


class RegionEndPointData(object):
    """class method endpoints data"""

    def __init__(self, region_id=None, public_url=None, end_point_type=None):
        """init function.

        :param public_url: field
        :param type: field
        :return:
        """
        self.region = region_id
        self.type = end_point_type
        self.public_url = public_url
