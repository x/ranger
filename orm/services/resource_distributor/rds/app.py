import logging
import os
import sys

from .services import region_resource_id_status
from .storage import factory
from oslo_config import cfg

from orm.common.client.audit.audit_client.api import audit
from pecan import conf, make_app
from pecan.commands import CommandRunner


logger = logging.getLogger(__name__)


def setup_app(pecan_config):
    """This method is the starting point of the application.
    The application can be started either by running pecan
    and pass it the config.py,
    or by running this file with python,
    then the main method is called and starting pecan.

    The method initializes components and return a WSGI application
    """

    init_audit()

    factory.database = conf.database
    region_resource_id_status.config = conf.region_resource_id_status

    app = make_app(conf.app.root, logging=conf.logging)
    logger.info('Starting RDS...')

    return app


def init_audit():
    """Initialize audit client module
    """
    audit.init(conf.audit.audit_server_url,
               conf.audit.num_of_send_retries,
               conf.audit.time_wait_between_retries,
               conf.app.service_name)


def main(argv=None):
    if argv is None:
        argv = sys.argv
    cfg.CONF(argv[1:], project='ranger', validate_default_values=True)

    dir_name = os.path.dirname(__file__)
    drive, path_and_file = os.path.splitdrive(dir_name)
    path, filename = os.path.split(path_and_file)
    runner = CommandRunner()
    runner.run(['serve', path + '/config.py'])


if __name__ == "__main__":
    main()
