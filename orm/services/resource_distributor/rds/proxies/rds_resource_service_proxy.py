import json
import logging
from pecan import conf
import requests

from orm.common.orm_common.utils.error_base import ErrorStatus
from orm.services.customer_manager.cms_rest.data.data_manager \
    import DataManager as CmsDataManager
from orm.services.flavor_manager.fms_rest.data.sql_alchemy.data_manager \
    import DataManager as FmsDataManager
from orm.services.image_manager.ims.persistency.sql_alchemy.data_manager \
    import DataManager as ImsDataManager
from orm.services.resource_distributor.rds.utils import \
    authentication as AuthService


logger = logging.getLogger(__name__)

headers = {'content-type': 'application/json'}


def _set_headers(region):
    try:
        token_id = AuthService.get_token(region)
        if token_id:
            headers['X-Auth-Token'] = token_id
            headers['X-Auth-Region'] = region
    except Exception:
        logger.error("no token")


def invoke_resources_region_delete(resource_type, region, resource_id):
    logger.debug("REGION STATUS PROXY - delete {} resource with ID:{} for "
                 "region {}".format(resource_type, resource_id, region))

    _set_headers(region)

    try:
        if resource_type == "customer":
            delete_customer_region(resource_id, region)
        elif resource_type == "group":
            delete_group_region(resource_id, region)
        elif resource_type == "flavor":
            delete_flavor_region(resource_id, region)
        elif resource_type == "image":
            delete_image_region(resource_id, region)

    except Exception as exp:
        logger.error(exp)
        raise ErrorStatus(
            "Fail to delete {} resource {}".format(resource_type, str(exp)))
    return


def delete_customer_region(customer_id, region_id):
    datamanager = CmsDataManager()
    try:
        customer_region = datamanager.get_record('customer_region')
        cust_regions = customer_region.get_regions_for_customer(customer_id)

        for region in cust_regions:
            # when "force_delete" option is used on delete customer region
            # request, we don't need to execute the delete_region_for_customer
            # function as it has already been previously handled by the
            # force_delete option
            if region.region.name == region_id:
                customer_region.delete_region_for_customer(customer_id,
                                                           region_id)
                break

        datamanager.flush()
        datamanager.commit()
    except Exception as exp:
        logger.error("RDS CMS resource - Failed to delete region")
        datamanager.rollback()
        raise exp
    finally:
        datamanager.close()


def delete_group_region(group_uuid, region_name):
    datamanager = CmsDataManager()
    try:
        group_region = datamanager.get_record('groups_region')
        group_region.delete_region_for_group(group_uuid, region_name)
        datamanager.flush()
        datamanager.commit()
    except Exception as exp:
        logger.error("RDS Group resource - Failed to delete region")
        datamanager.rollback()
        raise exp
    finally:
        datamanager.close()


def delete_flavor_region(flavor_uuid, region_name):
    datamanager = FmsDataManager()
    try:
        flavor_rec = datamanager.get_record('flavor')
        sql_flavor = flavor_rec.get_flavor_by_id(flavor_uuid)

        # An empty 'flavor_regions' indicates that force_delete option was
        # used on delete flavor region request.   As the flavor is no longer
        # associated with any region, we can skip the remove_region function.
        if sql_flavor is None or not sql_flavor.flavor_regions:
            return

        sql_flavor.remove_region(region_name)
        datamanager.flush()
        datamanager.commit()
    except Exception as exp:
        logger.error("RDS FMS resource - Failed to delete region")
        datamanager.rollback()
        raise exp
    finally:
        datamanager.close()


def delete_image_region(image_uuid, region_name):
    datamanager = ImsDataManager()
    try:
        image_rec = datamanager.get_record('image')
        sql_image = image_rec.get_image_by_id(image_uuid)

        # An empty sql_image.regions indicates that force_delete option was
        # used on delete image region request.   As the image is no longer
        # associated with any region, we can skip the remove_region function.
        if sql_image is None or not sql_image.regions:
            return

        sql_image.remove_region(region_name)
        datamanager.flush()
        datamanager.commit()
    except Exception as exp:
        logger.error("RDS IMS resource - Failed to delete region")
        datamanager.rollback()
        raise exp
    finally:
        datamanager.close()


def send_image_metadata(meta_data, region, resource_id, action='post'):
    logger.debug(
        "IMS PROXY - send metadata to ims {} for region {}".format(meta_data,
                                                                   region))
    data_to_send = {
        "metadata": {
            "checksum": meta_data['checksum'],
            "virtual_size": meta_data['virtual_size'],
            "size": meta_data['size']
        }
    }

    _set_headers(region)
    data_to_send_as_json = json.dumps(data_to_send)
    logger.debug("sending the data to ims server post method ")
    logger.debug("ims server {0} path = {1}".format(
        conf.ims.base_url,
        conf.ims.metadata_path).format(resource_id, region))

    if action == 'post':
        try:
            response = requests.post(
                conf.ims.base_url + (conf.ims.metadata_path).format(
                    resource_id, region),
                data=data_to_send_as_json, headers=headers, verify=conf.verify)
            logger.debug("got response from ims {}".format(response))
        except requests.ConnectionError as exp:
            logger.error(exp)
            raise ErrorStatus(
                "fail to connect to server {}".format(exp.message))

    if response.status_code != 200:
        raise ErrorStatus(
            "Got error from rds server, code: {0} message: {1}".format(
                response.status_code, response.content))
    return
