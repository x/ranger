# Copyright (c) 2012 OpenStack Foundation
# All Rights Reserved.
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may
#  not use this file except in compliance with the License. You may obtain
#  a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#  License for the specific language governing permissions and limitations
#  under the License.

from configparser import ConfigParser
from oslo_config import cfg
import re
from sqlalchemy import create_engine
import sys

CONF = cfg.CONF


def execute_app_custom_sql(conn):
    """Execute custom SQL statements based on configuration.

    Generates custom SQL insert statements from configuration parameters
    contained in ranger.conf  The functions must use execute with
    parameters to avoid sql injection.

    Parameters:
    conn (sqlalchemy.db): connection object for the SQL database

    """
    config = ConfigParser()
    config.read(CONF.config_file)

    if config.has_option("rds", "customer_domain"):
        customer_domain = config.get("rds", "customer_domain")
        customer_domain = re.sub(r'[\'\"]', '', customer_domain).strip()

        # Insert custom domain name into cms_domain talbe.
        sql = 'insert ignore into cms_domain(name) values(%s)'
        conn.execute(sql, (customer_domain, ))

        # Update domain_name column in region table
        update_regions = "update region set domain_name = %s" \
            " where domain_name is NULL or length(domain_name) = 0"
        conn.execute(update_regions, customer_domain)

        # Update users_domain column in customer table
        update_customer_domain = "update customer set customer_domain = %s" \
            " where customer_domain is NULL or length(customer_domain) = 0"
        conn.execute(update_customer_domain, customer_domain)


def execute_purge_uuids_record(conn):
    """Execute custom SQL statements to purge records from uuids table.

    Parameters:
    conn (sqlalchemy.db): connection object for the SQL database

    """
    sql = 'delete from uuids where uuid_type=\'transaction\' and ' \
        'uuid not in (select transaction_id from transactions) and ' \
        'uuid not in (select tracking_id from transactions) and ' \
        'uuid not in (select resource_id from transactions);'

    conn.execute(sql)


def main(argv=None):

    if argv is None:
        argv = sys.argv
    cfg.CONF(argv[1:], project='ranger', validate_default_values=True)

    OrmOpts = [
        cfg.StrOpt('ranger_base',
                   default='/opt/stack/ranger',
                   help='Orm base directory.'),
    ]

    CONF.register_opts(OrmOpts)

    orm_database_group = cfg.OptGroup(name='database',
                                      title='Orm Database Options')
    OrmDatabaseGroup = [
        cfg.StrOpt('connection',
                   help='The SQLAlchemy connection string to use to connect to '
                        'the ORM database.',
                   secret=True),
        cfg.IntOpt('max_retries',
                   default=-1,
                   help='The maximum number of retries for database connection.')
    ]

    CONF.register_group(orm_database_group)
    CONF.register_opts(OrmDatabaseGroup, orm_database_group)

    sql_queries = []

    orm_dbs = [
        CONF.ranger_base + '/orm/services/audit_trail_manager/scripts/db_scripts/create_db.sql',
        CONF.ranger_base + '/orm/services/id_generator/scripts/db_scripts/db_create.sql',
        CONF.ranger_base + '/orm/services/resource_distributor/scripts/db_scripts/create_db.sql',
        CONF.ranger_base + '/orm/services/region_manager/scripts/db_scripts/create_db.sql',
        CONF.ranger_base
        + '/orm/services/customer_manager/scripts/db_scripts/ranger_cms_create_db.sql',
        CONF.ranger_base
        + '/orm/services/customer_manager/scripts/db_scripts/ranger_cms_update_db.sql',
        CONF.ranger_base
        + '/orm/services/flavor_manager/scripts/db_scripts/ranger_fms_create_db.sql',
        CONF.ranger_base
        + '/orm/services/flavor_manager/scripts/db_scripts/ranger_fms_update_db.sql',
        CONF.ranger_base + '/orm/services/image_manager/scripts/db_scripts/create_db.sql'
    ]

    for item in range(len(orm_dbs)):
        sql_file = open(orm_dbs[item], "r")
        query = sql_file.read()
        sql_queries.append(query)
        sql_file.close()

    db_conn_url = CONF.database.connection
    db_conn_url = db_conn_url and db_conn_url.replace("mysql+pymysql", "mysql") or ''
    engine = create_engine(db_conn_url, echo=False)

    for exec_item in range(len(sql_queries)):
        conn = engine.connect()
        exec_script = conn.execute(sql_queries[exec_item])
        conn.close()

    conn = engine.connect()
    execute_app_custom_sql(conn)
    execute_purge_uuids_record(conn)
    conn.close()
