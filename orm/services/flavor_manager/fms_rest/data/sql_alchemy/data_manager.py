import logging

from orm.services.flavor_manager.fms_rest.data.sql_alchemy.flavor.\
    flavor_record import FlavorRecord
from oslo_db.sqlalchemy.enginefacade import LegacyEngineFacade

from pecan import conf
from sqlalchemy.event import listen

LOG = logging.getLogger(__name__)


# event handling
def on_before_flush(session, flush_context, instances):
    for model in session.new:
        if hasattr(model, "validate"):
            model.validate("new")

    for model in session.dirty:
        if hasattr(model, "validate"):
            model.validate("dirty")


class DataManager(object):

    def __init__(self, connection_string=None):
        if not connection_string:
            connection_string = conf.database.connection_string

        try:
            self._engine_facade = LegacyEngineFacade(connection_string, autocommit=False)
        except Exception:
            nagios_message = "CRITICAL|CONDB001 - "
            err_message = "Could not establish database connection"
            LOG.error(nagios_message + err_message)
            raise Exception(err_message)

        # self._engine = create_engine(connection_string, echo=True)
        # self.session_maker = sessionmaker(bind=self.get_engine())
        self._session = None
        listen(self.session, 'before_flush', on_before_flush)
        self.flavor_record = None

    def get_engine(self):
        return self._engine_facade.get_engine()

    @property
    def engine(self):
        return self.get_engine()

    def get_session(self):
        if not self._session:
            self._session = self._engine_facade.get_session()
        return self._session

    @property
    def session(self):
        return self.get_session()

    def begin_transaction(self):
        # self.session.begin()
        # no need to begin transaction - the transaction is open automatically
        pass

    def flush(self):
        try:
            self.session.flush()
        except Exception as exp:
            raise

    def commit(self):
        self.session.commit()

    def rollback(self):
        self.session.rollback()

    def close(self):
        self.session.close()
        self.engine.dispose()

    def get_record(self, record_name):
        if record_name == "Flavor" or record_name == "flavor":
            # if not hasattr(self, "flavor_record"):
            #    self.flavor_record = FlavorRecord(self.session)
            if not self.flavor_record:
                self.flavor_record = FlavorRecord(self.session)
            return self.flavor_record
        return None

    def get_valid_tenant_region_list(self, requested_tenants,
                                     requested_regions):

        datamanager = DataManager()
        # convert tenant and region lists to sql-friendly list format
        tenants_list = str(tuple(requested_tenants)).rstrip(',)') + ')'
        regions_list = str(tuple(requested_regions)).rstrip(',)') + ')'

        # --- use sql query for processing time considerations
        # get valid tenants list for the region(s) by checking
        # tenant/region status in resource_status table if
        # status shows 'Success' and operation not 'delete'
        sql_query = '''select a.uuid, b.name from customer a,
            cms_region b, customer_region c, resource_status d
            where c.customer_id = a.id and c.region_id = b.id
            and d.resource_id = a.uuid and d.region = b.name
            and ((d.status = 'Success' and d.operation != 'delete')
            or (d.status = 'Error' and d.operation = 'modify'))
            and a.uuid in %s and b.name in %s'''

        results = datamanager.session.connection().execute(sql_query % (tenants_list, regions_list)).fetchall()

        return results

    def get_valid_tenant_list(self, requested_tenants):
        # validate if tenants in list exist in customer table

        datamanager = DataManager()
        # convert tenant and region lists to sql-friendly list format
        tenants_list = str(tuple(requested_tenants)).rstrip(',)') + ')'
        sql_query = '''select uuid from customer where uuid in %s'''
        valid_tenants_list = datamanager.session.connection().execute(sql_query % (tenants_list)).fetchall()

        return valid_tenants_list
