import json
import pprint
import requests

from orm.common.orm_common.injector import injector
from orm.common.orm_common.utils.error_base import ErrorStatus
from orm.services.flavor_manager.fms_rest.logger import get_logger

from pecan import conf, request

di = injector.get_di()

LOG = get_logger(__name__)

headers = {'content-type': 'application/json'}


def send_flavor(flavor_dict, transaction_id, action="put"):
    # action can be "post" for creating flavor or "delete" for deleting flavor

    data = {
        "service_template": {
            "resource": {
                "resource_type": "flavor"
            },
            "model": str(json.dumps(flavor_dict)),
            "tracking": {
                "external_id": "",
                "tracking_id": transaction_id
            }
        }
    }

    data_to_display = {
        "service_template": {
            "resource": {
                "resource_type": "flavor"
            },
            "model": flavor_dict,
            "tracking": {
                "external_id": "",
                "tracking_id": transaction_id
            }
        }
    }

    pp = pprint.PrettyPrinter(width=30)
    pretty_text = pp.pformat(data_to_display)
    wrapper_json = json.dumps(data)

    headers['X-RANGER-Client'] = request.headers[
        'X-RANGER-Client'] if 'X-RANGER-Client' in request.headers else \
        'NA'
    headers['X-RANGER-Requester'] = request.headers[
        'X-RANGER-Requester'] if 'X-RANGER-Requester' in request.headers else \
        ''
    headers['X-Auth-Region'] = request.headers[
        'X-Auth-Region'] if 'X-Auth-Region' in \
        request.headers else ''
    headers['X-Auth-Token'] = request.headers[
        'X-Auth-Token'] if 'X-Auth-Token' in \
        request.headers else ''

    LOG.debug("Wrapper JSON before sending action: {0} to Rds Proxy {1}".format(action, pretty_text))
    LOG.info("Sending to RDS Server: " + conf.api.rds_server.base + conf.api.rds_server.resources)
    if action == "post":
        resp = requests.post(conf.api.rds_server.base + conf.api.rds_server.resources,
                             data=wrapper_json,
                             headers=headers,
                             verify=conf.verify)
    elif action == "put":
        resp = requests.put(conf.api.rds_server.base + conf.api.rds_server.resources,
                            data=wrapper_json,
                            headers=headers,
                            verify=conf.verify)
    elif action == "delete":
        resp = requests.delete(conf.api.rds_server.base + conf.api.rds_server.resources,
                               data=wrapper_json,
                               headers=headers,
                               verify=conf.verify)
    else:
        raise Exception("RdsProxy.send_flavor action_type invalid."
                        " Action can be post or delete, got {0}".format(action))

    content = resp.content
    LOG.debug("return from rds server status code: {0} content: {1}".format(
        resp.status_code,
        resp.content))
    if resp.content and 200 <= resp.status_code < 300:
        content = resp.json()
    else:
        raise ErrorStatus("Got error from rds server: {0}".format(
            resp.json()['faultstring']),
            resp.status_code)

    return content


def get_status(resource_id):
    try:
        LOG.debug("Calling to RDS Server to get status")
        resp = requests.get(conf.api.rds_server.base + conf.api.rds_server.status + resource_id, verify=conf.verify)
        pp = pprint.PrettyPrinter(width=30)
        pretty_text = pp.pformat(resp.json())
        LOG.debug("Response from RDS Server:\n" + pretty_text)
        return resp

    except Exception as exp:
        LOG.log_exception("FlavorLogic - Failed to Get status for flavor: " + resource_id, exp)
        raise
