import logging

from orm.services.id_generator.uuidgen.db.uuid_db import UUID
from oslo_db.sqlalchemy.enginefacade import LegacyEngineFacade
from pecan import conf

logger = logging.getLogger(__name__)


class DBManager(object):

    def __init__(self, connection_string=None):
        if not connection_string:
            connection_string = conf.database.connection_string

        self._engine_facade = LegacyEngineFacade(connection_string,
                                                 autocommit=False)
        self._session = None

    def get_session(self):
        if not self._session:
            self._session = self._engine_facade.get_session()
        return self._session

    @property
    def session(self):
        return self.get_session()

    def begin_transaction(self):
        # self.session.begin()
        # no need to begin transaction - the transaction is open automatically
        pass

    def get_engine(self):
        return self._engine_facade.get_engine()

    @property
    def engine(self):
        return self.get_engine()

    def close(self):
        self.session.close()
        self.engine.dispose()

    def create_uuid(self, _uuid, _uuid_type):
        uuid = UUID()
        uuid.uuid = _uuid
        uuid.uuid_type = _uuid_type

        try:
            self.begin_transaction()
            self.session.add(uuid)
            self.session.commit()
            self.session.close()
            self.engine.dispose()
        except Exception as ex:
            logger.exception(
                "Error adding uuid {} : {}".format(_uuid, str(ex)))
            raise ex

    def get_uuid_details(self, _uuid):
        try:
            self.begin_transaction()
            uuid = self.session.query(UUID).filter(UUID.uuid == _uuid)
            self.close()
            return uuid.first()
        except Exception as ex:
            logger.exception(
                "Error getting uuid {} : {}".format(_uuid, str(ex)))
            raise ex

    def delete_uuid(self, _uuid):
        try:
            self.begin_transaction()
            result = self.session.query(UUID).filter(
                UUID.uuid == _uuid).delete()
            self.session.commit()
            self.close()
            return result
        except Exception as ex:
            logger.exception(
                "Error deleting uuid {} : {}".format(_uuid, str(ex)))
            raise ex
