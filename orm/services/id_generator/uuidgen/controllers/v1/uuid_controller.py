import datetime
import logging
import re
import uuid

from pecan import expose, request, response
from pecan.rest import RestController

from orm.common.orm_common.utils.error_base import NotFoundError
from orm.common.orm_common.utils import utils
from orm.services.id_generator.uuidgen.db.db_manager import DBManager
from orm.services.id_generator.uuidgen.utils import authentication

LOG = logging.getLogger(__name__)


def respond(reason, code, message):
    """A helper function to create a response dict with the given values"""
    return {
        reason: {
            "code": code,
            "message": message
        }
    }


class UUIDController(RestController):
    @expose(template='json')
    def get_one(self, uuid):
        LOG.info("UUIDController.get_one uuid=" + uuid)
        try:
            db_manager = DBManager()
            results = db_manager.get_uuid_details(uuid)
            if results is None:
                response.status = 404
                message = "UUID {} not found".format(uuid)
                messageToReturn = respond(
                    "NotFoundError", response.status, message)
                return messageToReturn

            return results.get_dict()
        except Exception as e:
            response.status = 500
            messageToReturn = respond(
                "BadRequest", response.status, 'Database error')
            LOG.error(str(messageToReturn) + "Exception: " + str(e))
            return messageToReturn

    @expose(template='json')
    def delete(self, uuid):
        LOG.info("UUIDController.delete uuid=" + uuid)

        # authenticate the request
        try:
            self.validate_headers()
            db_manager = DBManager()
            db_session = db_manager.get_session()

            # Get keystone endpoint so that authientication.authorize()
            # does not need to request rms service for the endpoint
            auth_region = request.headers.get('X-Auth-Region')
            keystone_ep = utils.get_region_end_point_from_db(
                db_session, auth_region, 'identity')

            if keystone_ep is None:
                raise NotFoundError

            authentication.authorize(request, 'uuid:delete', keystone_ep)

        except KeyError as e:
            response.status = 400
            message = "Missing mandatory parameters - {}".format(str(e))
            messageToReturn = respond(
                "MissingParams", response.status, message)
            return messageToReturn

        except NotFoundError as e:
            response.status = 404
            message = "EP for region:{} type:identity not found".format(
                auth_region)
            messageToReturn = respond(
                "NotFoundError", response.status, message)
            return messageToReturn

        except Exception as e:
            response.status = 401
            message = "Failed to authenticate. UUID {} not deleted. {}".format(
                uuid, str(e))
            messageToReturn = respond(
                "FailedAuthentication", response.status, message)
            LOG.error(str(messageToReturn) + " Exception: " + str(e))
            return messageToReturn

        try:
            results = db_manager.delete_uuid(uuid)
            if results == 0:
                response.status = 404
                message = "UUID {} not found".format(uuid)
                messageToReturn = respond(
                    "FailedDeletion", response.status, message)
                return messageToReturn
            return {}
        except Exception as e:
            response.status = 500
            message = "Database error - {}".format(str(e))
            messageToReturn = respond(
                "ServerError", response.status, message)
            LOG.error(str(messageToReturn) + "Exception: " + str(e))
            return messageToReturn

    @expose(template='json')
    def post(self, **kw):
        """Method to handle POST /v1/uuids - create and return a new uuid
            prameters:
                uuid_type (optional)
            return: dict describing success or failure of post command
        """
        messageToReturn = None
        uuid_type = ''
        customer_id = None

        if 'uuid_type' in kw:
            uuid_type = kw['uuid_type']
            del kw['uuid_type']
        if 'uuid' in kw:
            customer_id = kw['uuid']
            del kw['uuid']
        LOG.info("UUIDController.post (url: /v1/uuids) uuid_type=" + uuid_type)

        if len(kw):
            response.status = 400
            messageToReturn = respond("BadRequest", 400, 'Unknown parameter(s):' + ', '.join(list(kw.keys())))
            LOG.info("UUIDController.post - " + str(messageToReturn))
            return messageToReturn

        if not customer_id or customer_id == 'Unset':
            return self.create_new_uuid(uuid_type)

        if not re.match(r'^[A-Za-z0-9]+$', customer_id):
            response.status = 400
            messageToReturn = respond("BadRequest", 400, "Only alphanumeric characters allowed!")
            LOG.info("UUIDController.post - " + str(messageToReturn))
            return messageToReturn

        return self.validate_and_add_uuid(customer_id, uuid_type)

    def validate_and_add_uuid(self, uuid, uuid_type):
        try:
            db_manager = DBManager()
            db_manager.create_uuid(uuid, uuid_type)
            return {
                "uuid": uuid,
                "issued_at": datetime.datetime.utcnow().isoformat() + 'Z',
                "uuid_type": uuid_type
            }
        except Exception as e:
            # if create_uuid detects duplicate UUID error we will throw
            # exception in the calling service (cms, fms, or ims)
            if 'Duplicate entry' in str(e):
                response.status = 409
                messageToReturn = respond('Duplicate Entry', response.status,
                                          'Duplicate UUID {}'.format(uuid))
                LOG.error(str(messageToReturn) + str(e))
                return messageToReturn
            else:
                response.status = 500
                messageToReturn = respond(
                    'BadRequest', response.status, 'Database error')
                LOG.error(str(messageToReturn) + str(e))
                return messageToReturn

    def create_new_uuid(self, uuid_type):
        uu = uuid.uuid4().hex

        try:
            db_manager = DBManager()
            db_manager.create_uuid(uu, uuid_type)
            return {
                "uuid": uu,
                "issued_at": datetime.datetime.utcnow().isoformat() + 'Z',
                "uuid_type": uuid_type
            }
        except Exception as e:
            response.status = 500
            messageToReturn = respond("BadRequest", 500, 'Database error')
            LOG.error(str(messageToReturn) + "Exception: " + str(e))
            return messageToReturn

    def validate_headers(self):
        if request.headers.get('X-Auth-Region') is None:
            raise KeyError("Header missing X-Auth-Region")

        if request.headers.get('X-Auth-Token') is None:
            raise KeyError("Header missing X-Auth-Token")
