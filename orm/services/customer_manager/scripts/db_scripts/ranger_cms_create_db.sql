SET sql_notes=0;

use orm;

create table if not exists cms_role
   (
	 id integer auto_increment not null,
	 name varchar(64) not null,
	 primary key (id));

create table if not exists cms_user
   (
	 id integer auto_increment not null,
	 name varchar(64) not null,
	 primary key (id),
	 unique name_idx (name));

create table if not exists cms_region
   (
	 id integer auto_increment not null,
	 name varchar(64) not null,
	 type varchar(64) not null DEFAULT 'single',
	 primary key (id),
	 unique name_idx (name));


create table if not exists customer
   (
	 id integer auto_increment not null,
	 uuid varchar(64) not null,
 	 name varchar(64) not null,
	 description varchar(255) not null,
	 customer_domain varchar(64) not null,
	 enabled tinyint not null,
	 primary key (id),
	 unique uuid_idx (uuid),
	 unique name_idx(name));

create table if not exists customer_metadata
	(
	 customer_id integer not null,
	 field_key varchar(64) not null,
	 field_value varchar(64) not null,
	 primary key (customer_id, field_key),
	 foreign key (customer_id) references customer(id) ON DELETE CASCADE
	);

create table if not exists customer_region
   (
	 customer_id integer not null,
	 region_id integer not null,
	 primary key (customer_id,region_id),
	 index region_id (region_id),
  	 foreign key (customer_id) REFERENCES `customer` (`id`) ON DELETE CASCADE,
	 foreign key (region_id) REFERENCES `cms_region` (`id`));

create table if not exists quota
   (
	 id integer auto_increment not null,
	 customer_id integer not null,
	 region_id integer not null,
	 quota_type varchar(64) not null,
	 foreign key (region_id) references cms_region(id),
	 primary key (id),
	 unique quota_type (customer_id,region_id,quota_type),
	 foreign key (`customer_id`, `region_id`) REFERENCES `customer_region` (`customer_id`, `region_id`) ON DELETE CASCADE ON UPDATE NO ACTION
   );

create table if not exists quota_field_detail
   (
	 id integer auto_increment not null,
	 quota_id integer not null,
	 field_key varchar(64) not null,
	 field_value varchar(64) not null,
	 primary key (id),
	 foreign key (quota_id) references quota(id) ON DELETE CASCADE,
	 unique key_idx (quota_id,field_key));

create table if not exists user_role
   (
	 customer_id integer not null,
	 region_id integer not null,
	 user_id integer not null,
	 role_id integer not null,
	 primary key (customer_id,region_id,user_id,role_id),
	 foreign key (customer_id, region_id) REFERENCES customer_region (`customer_id`, `region_id`) ON DELETE CASCADE,
	 foreign key (customer_id) references customer(id) ON DELETE CASCADE,
	 foreign key (region_id) references cms_region(id),
	 foreign key (user_id) references cms_user(id),
	 foreign key (role_id) references cms_role(id),
	 index region_id (region_id),
	 index user_id (user_id));

create table if not exists cms_domain
   (
         id integer auto_increment not null,
         name varchar(64) not null,
         primary key (id),
         unique name_idx (name));

create table if not exists groups
   (
         id integer auto_increment not null,
         uuid varchar(64) not null,
         domain_name varchar(64) not null,
         name varchar(64) not null,
         description varchar(255) not null,
         enabled tinyint not null,
         primary key (id),
         foreign key (`domain_name`) references `cms_domain` (`name`) ON DELETE CASCADE ON UPDATE NO ACTION,
         unique name_idx(name),
         unique uuid_idx (uuid));


create table if not exists groups_region
   (
         region_id integer not null,
         group_id varchar(64) not null,
         primary key (region_id, group_id),
         foreign key (`region_id`) references `cms_region` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
         foreign key (`group_id`) references `groups` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION,
         index region_id (region_id),
         index group_id_idx (group_id));

create table if not exists groups_role
   (
         role_id integer not null,
         group_id varchar(64) not null,
         primary key (role_id, group_id),
	 foreign key (role_id) references cms_role(id),
         foreign key (`group_id`) references `groups` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION,
         index group_id_idx (group_id));

create table if not exists groups_user
   (
         group_id varchar(64) not null,
         region_id integer not null,
         user_id integer not null,
         domain_name varchar(64) not null,
         primary key (group_id, region_id, domain_name, user_id),
         foreign key (`user_id`) references `cms_user` (`id`) ON DELETE CASCADE,
         foreign key (`group_id`) references `groups` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION,
         foreign key (`group_id`,`region_id`) references `groups_region` (`group_id`,`region_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
         foreign key (`domain_name`) references `cms_domain` (`name`) ON DELETE CASCADE ON UPDATE NO ACTION,
         index group_id (group_id),
         index region_id (region_id),
         index domain_name (domain_name));

create table if not exists groups_customer_role
   (
         group_id varchar(64) not null,
         customer_id integer not null,
         region_id integer not null,
         role_id integer not null,
         primary key (group_id, customer_id, region_id, role_id),
         foreign key (`group_id`) references `groups` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION,
         foreign key (`customer_id`) references `customer` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
         foreign key (`group_id`, `role_id`) references `groups_role` (`group_id`, `role_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
         foreign key (`region_id`) references `cms_region` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
         index customer_id_idx (customer_id),
         index role_id_idx (role_id));

create table if not exists groups_domain_role
   (
         group_id varchar(64) not null,
         domain_name varchar(64) not null,
         region_id integer not null,
         role_id integer not null,
         primary key (group_id, domain_name, region_id, role_id),
         foreign key (`group_id`) references `groups` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION,
         foreign key (`domain_name`) references `cms_domain` (`name`) ON DELETE CASCADE ON UPDATE NO ACTION,
         foreign key (`group_id`, `role_id`) references `groups_role` (`group_id`, `role_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
         foreign key (`region_id`) references `cms_region` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
         index role_id_idx (role_id));
