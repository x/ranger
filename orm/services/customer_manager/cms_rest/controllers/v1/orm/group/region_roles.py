from oslo_db.exception import DBDuplicateEntry
from pecan import request, rest
from wsmeext.pecan import wsexpose

from orm.common.orm_common.utils import api_error_utils as err_utils
from orm.common.orm_common.utils.error_base import ErrorStatus
from orm.common.orm_common.utils import utils
from orm.services.customer_manager.cms_rest.logger import get_logger
from orm.services.customer_manager.cms_rest.logic.group_logic import GroupLogic
from orm.services.customer_manager.cms_rest.model.GroupModels import \
    RoleAssignment, RoleResultWrapper
from orm.services.customer_manager.cms_rest.utils import authentication

LOG = get_logger(__name__)


class RegionRoleController(rest.RestController):

    @wsexpose(str, str, str, rest_content_types='json')
    def get(self, group_id, region):
        return "This is groups region role controller group id:{0} " \
               "region:{1}".format(group_id, region)

    @wsexpose(RoleResultWrapper, str, str, body=[RoleAssignment],
              rest_content_types='json', status_code=200)
    def post(self, group_id, region, role_assignments):
        LOG.info("RegionRoleController - Assign Roles to group id [{0}] for "
                 "region [{1}] roles [{2}]".format(
                     group_id, region, str(role_assignments)))
        authentication.authorize(request, 'groups:assign_region_role')
        try:
            group_logic = GroupLogic()
            result = group_logic.assign_roles(group_id,
                                              role_assignments,
                                              request.transaction_id,
                                              region)
            LOG.info("RegionRoleController - Roles assigned: " + str(result))

            event_details = 'Group {} - region roles assigned'.format(group_id)
            utils.audit_trail('assigned group region roles',
                              request.transaction_id,
                              request.headers,
                              group_id,
                              event_details=event_details)

        except DBDuplicateEntry as exception:
            LOG.log_exception(
                "DBDuplicateEntry-Group Region Roles already assigned.",
                exception)
            raise err_utils.get_error(
                request.transaction_id,
                status_code=409.2,
                message=exception.message)

        except ErrorStatus as exception:
            LOG.log_exception(
                "ErrorStatus - Failed to assign roles for region.", exception)
            raise err_utils.get_error(request.transaction_id,
                                      message=exception.message,
                                      status_code=exception.status_code)
        except Exception as exception:
            LOG.log_exception(
                "Exception - Failed in assign roles for region.", exception)
            raise err_utils.get_error(request.transaction_id,
                                      status_code=500,
                                      message=str(exception))

        return result
