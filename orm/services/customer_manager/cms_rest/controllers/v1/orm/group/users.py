from oslo_db.exception import DBDuplicateEntry
from pecan import request, rest
from wsmeext.pecan import wsexpose

from orm.common.orm_common.utils import api_error_utils as err_utils
from orm.common.orm_common.utils import utils
from orm.services.customer_manager.cms_rest.logger import get_logger
from orm.common.orm_common.utils.error_base import \
    ErrorStatus, NotFoundError, ConflictError
from orm.services.customer_manager.cms_rest.logic.group_logic import GroupLogic
from orm.services.customer_manager.cms_rest.model.GroupModels import \
    User, UserResultWrapper
from orm.services.customer_manager.cms_rest.utils import authentication

LOG = get_logger(__name__)


class UserController(rest.RestController):

    @wsexpose([str], str, rest_content_types='json')
    def get(self, group_id):
        return ["This is groups user controller ", "group id: " + group_id]

    @wsexpose(UserResultWrapper, str, body=[User],
              rest_content_types='json', status_code=200)
    def post(self, group_id, users):
        LOG.info("UserController - Add Default users to group id {0} "
                 "users: {1}".format(group_id, str(users)))
        authentication.authorize(request, 'groups:add_group_default_users')
        try:
            group_logic = GroupLogic()
            result = \
                group_logic.add_group_default_users(group_id,
                                                    users,
                                                    request.transaction_id)

            LOG.info("UserController - Users added: " + str(result))

            event_details = 'Group {} - users assigned.'.format(group_id)
            utils.audit_trail('added group users',
                              request.transaction_id,
                              request.headers,
                              group_id,
                              event_details=event_details)

        except (ConflictError, DBDuplicateEntry) as exception:
            LOG.log_exception(
                "DBDuplicateEntry - Group users already assigned.", exception)
            raise err_utils.get_error(
                request.transaction_id,
                status_code=409.2,
                message=exception.message)

        except (NotFoundError, ErrorStatus) as exception:
            LOG.log_exception(
                "ErrorStatus - Failed to add users", exception)
            raise err_utils.get_error(request.transaction_id,
                                      message=exception.message,
                                      status_code=exception.status_code)
        except Exception as exception:
            LOG.log_exception(
                "Exception - Failed in add default users", exception)
            raise err_utils.get_error(request.transaction_id,
                                      status_code=500,
                                      message=str(exception))

        return result

    @wsexpose(None, str, str, str, status_code=204)
    def delete(self, group_id, user, user_domain):

        requester = request.headers.get('X-RANGER-Requester')
        is_rds_client_request = requester == 'rds_resource_service_proxy'

        authentication.authorize(request, 'groups:delete_group_default_user')
        try:
            group_logic = GroupLogic()
            group_logic.delete_group_default_user(group_id,
                                                  user,
                                                  user_domain,
                                                  request.transaction_id)

            LOG.info("UserController - Remove user from group finished")

            event_details = 'Group {} users unassigned'.format(group_id)
            utils.audit_trail('delete group user',
                              request.transaction_id,
                              request.headers,
                              group_id,
                              event_details=event_details)

        except ValueError as exception:
            raise err_utils.get_error(request.transaction_id,
                                      message=str(exception),
                                      status_code=404)

        except (NotFoundError, ErrorStatus) as exception:
            LOG.log_exception("ErrorStatus - Failed to delete user from group",
                              exception)
            raise err_utils.get_error(request.transaction_id,
                                      message=exception.message,
                                      status_code=exception.status_code)

        except Exception as exception:
            LOG.log_exception("Exception - Failed in delete default user",
                              exception)
            raise err_utils.get_error(request.transaction_id,
                                      status_code=500,
                                      message=str(exception))
