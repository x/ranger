from oslo_db.exception import DBDuplicateEntry
from pecan import request, rest
from wsmeext.pecan import wsexpose

from orm.common.orm_common.utils import api_error_utils as err_utils
from orm.common.orm_common.utils import utils
from orm.services.customer_manager.cms_rest.logger import get_logger
from orm.common.orm_common.utils.error_base import \
    ErrorStatus, NotFoundError, ConflictError
from orm.services.customer_manager.cms_rest.logic.group_logic import GroupLogic
from orm.services.customer_manager.cms_rest.model.GroupModels import \
    RegionUser, RegionUserResultWrapper
from orm.services.customer_manager.cms_rest.utils import authentication

LOG = get_logger(__name__)


class RegionUserController(rest.RestController):

    @wsexpose([str], str, str, str, rest_content_types='json')
    def get(self, group_id, region_id, user_id):
        return ["This is groups region user controller for group id: "
                + group_id]

    @wsexpose(RegionUserResultWrapper, str, str, body=[RegionUser],
              rest_content_types='json', status_code=200)
    def post(self, group_id, region_id, users):
        LOG.info("RegionUserController - Add users to group id {0} "
                 "region_id : {1}".format(group_id, region_id))
        authentication.authorize(request, 'groups:add_group_region_users')
        try:
            group_logic = GroupLogic()
            result = group_logic.add_group_region_users(group_id,
                                                        region_id,
                                                        users,
                                                        request.transaction_id)

            LOG.info("RegionUserController - Users added: " + str(result))

            event_details = 'Group {} - users assigned.'.format(group_id)
            utils.audit_trail('added group users',
                              request.transaction_id,
                              request.headers,
                              group_id,
                              event_details=event_details)

        except (ConflictError, DBDuplicateEntry) as exception:
            LOG.log_exception(
                "Duplicate Entry - Group users already assigned.", exception)
            raise err_utils.get_error(
                request.transaction_id,
                status_code=409.2,
                message=exception.message)

        except (NotFoundError, ErrorStatus) as exception:
            LOG.log_exception(
                "ErrorStatus - Failed to add users", exception)
            raise err_utils.get_error(request.transaction_id,
                                      message=exception.message,
                                      status_code=exception.status_code)
        except Exception as exception:
            LOG.log_exception(
                "Exception - Failed in add region users", exception)
            raise err_utils.get_error(request.transaction_id,
                                      status_code=500,
                                      message=str(exception))

        return result

    @wsexpose(None, str, str, str, str, status_code=204)
    def delete(self, group_id, region_id, user, user_domain):
        requester = request.headers.get('X-RANGER-Requester')
        is_rds_client_request = requester == 'rds_resource_service_proxy'
        LOG.info("Remove users from group id: {0} user: {1} ".format(
                 group_id, user))

        authentication.authorize(request, 'groups:delete_group_region_user')
        try:
            group_logic = GroupLogic()
            group_logic.delete_group_region_user(group_id,
                                                 region_id,
                                                 user,
                                                 user_domain,
                                                 request.transaction_id)

            LOG.info("UserController - Remove user from group finished")

            event_details = 'Group {} users unassigned'.format(group_id)
            utils.audit_trail('delete group user',
                              request.transaction_id,
                              request.headers,
                              group_id,
                              event_details=event_details)

        except ValueError as exception:
            raise err_utils.get_error(request.transaction_id,
                                      message=str(exception),
                                      status_code=404)
        except ErrorStatus as exception:
            LOG.log_exception("ErrorStatus - Failed to delete user from group",
                              exception)
            raise err_utils.get_error(request.transaction_id,
                                      message=exception.message,
                                      status_code=exception.status_code)

        except NotFoundError as e:
            raise err_utils.get_error(request.transaction_id,
                                      message=e.message,
                                      status_code=404)

        except Exception as exception:
            LOG.log_exception("Exception - Failed in delete default user",
                              exception)
            raise err_utils.get_error(request.transaction_id,
                                      status_code=500,
                                      message=str(exception))
