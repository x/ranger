from orm.common.orm_common.utils.error_base import NotFoundError
from orm.services.customer_manager.cms_rest.data.sql_alchemy.cms_user_record \
    import CmsUserRecord
from orm.services.customer_manager.cms_rest.data.sql_alchemy.models \
    import GroupsUser
from orm.services.customer_manager.cms_rest.data.sql_alchemy.region_record \
    import RegionRecord
from orm.services.customer_manager.cms_rest.logger import get_logger

LOG = get_logger(__name__)


class GroupsUserRecord:
    def __init__(self, session):

        # thie model uses for the parameters for any acceess methods - not
        # as instance of record in the table
        self.__groups_user = GroupsUser()
        self.__TableName = "groups_user"

        if (session):
            self.session = session

    def setDBSession(self, session):
        self.session = session

    @property
    def groups_user(self):
        return self.__groups_user

    @groups_user.setter
    def groups_user(self):
        self.__groups_user = GroupsUser()

    def insert(self, groups_user):
        try:
            self.session.add(groups_user)
        except Exception as exception:
            LOG.log_exception(
                "Failed to insert groups_user" + str(groups_user), exception)
            raise

    def get_groups_user_keys(self,
                             user_name,
                             group_uuid,
                             region_name,
                             domain_name):
        region_record = RegionRecord(self.session)
        region_id = region_record.get_region_id_from_name(region_name)

        if region_id is None:
            raise ValueError(
                'region with the region name {0} not found'.format(
                    region_name))

        user_record = CmsUserRecord(self.session)
        user_id = user_record.get_cms_user_id_from_name(user_name)

        if user_id is None:
            raise ValueError(
                'user with the user name {0} not found'.format(
                    user_name))
        try:
            group = self.session.query(GroupsUser).filter(
                and_(
                    GroupsUser.group_id == group_uuid,
                    GroupsUser.domain_name == domain_name,
                    GroupsUser.region_id == region_id,
                    GroupsUser.user_id == user_id))
            return group.first()

        except Exception as exception:
            message = "Failed to get groups user by primary keys: " \
                " group_uuid:%s domain:%s region:%s user_name: %s" \
                % group_uuid, domain_name, region_name, user_name
            LOG.log_exception(message, exception)
            raise

    def get_users_for_group(self, group_uuid):
        groups_users = []

        try:
            query = self.session.query(GroupsUser).filter(
                GroupsUser.group_id == group_uuid)

            for groups_user in query.all():
                groups_users.append(groups_user)
            return groups_users

        except Exception as exception:
            message = "Failed to get users for group: %s" % (group_uuid)
            LOG.log_exception(message, exception)
            raise

    def remove_user_from_group(self,
                               group_uuid,
                               region_id,
                               domain,
                               user_id):

        # Check if 'region_id' is a string -  if so, get corresponding
        # cms_region id value for use later to query/delete the
        # corresponding group user record
        if isinstance(region_id, str):
            region_query = region_id
            region_record = RegionRecord(self.session)
            region_id = region_record.get_region_id_from_name(region_id)
            if region_id is None:
                raise NotFoundError("region {} ".format(region_query))

        # get cms_user id value for user_id (contains user name)
        # to query/delete the corresponding group user record
        user_name = user_id
        cms_user_record = CmsUserRecord(self.session)
        user_id = cms_user_record.get_cms_user_id_from_name(user_id)
        if user_id is None:
            raise NotFoundError("user {} ".format(user_name))

        # when deleting user from a specific region, verify that user
        # is associated with the group and region in the delete request
        if region_id > -1:
            user_check = 'SELECT DISTINCT user_id from groups_user \
                          WHERE group_id =%s AND region_id =%s \
                          AND user_id =%s AND domain_name =%s'

            result = self.session.connection().execute(user_check,
                                                       group_uuid,
                                                       region_id,
                                                       user_id, domain)
            if result.rowcount == 0:
                raise NotFoundError("user {}@{} domain".format(user_name, domain))

        if region_id == -1:
            cmd = "DELETE ur FROM groups_user ur,groups_user u \
                   WHERE ur.user_id=u.user_id \
                   AND ur.domain_name=u.domain_name \
                   AND ur.group_id = u.group_id AND u.region_id =-1 \
                   AND ur.group_id = %s  AND ur.domain_name= %s \
                   AND ur.user_id= %s"
            result = self.session.connection().execute(cmd,
                                                       group_uuid,
                                                       domain,
                                                       user_id)

        else:
            # DELETE command to identify whether or not the provided region
            # user/user_domain combo is also a default user/user_domain for
            # the group; if it is, NO group_user record(s) will be deleted
            del_cmd = "DELETE ur FROM groups_user as ur \
                       LEFT JOIN groups_user AS u \
                       ON ur.group_id = u.group_id AND u.user_id=ur.user_id \
                       AND u.region_id =-1 AND ur.domain_name = u.domain_name \
                       WHERE ur.group_id = %s AND ur.region_id= %s \
                       AND ur.user_id= %s AND ur.domain_name = %s \
                       AND u.user_id IS NULL"

            result = self.session.connection().execute(del_cmd,
                                                       group_uuid,
                                                       region_id,
                                                       user_id,
                                                       domain)
        return result
