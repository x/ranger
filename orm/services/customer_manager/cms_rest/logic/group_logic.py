import oslo_db
from pecan import conf, request
import requests

from orm.common.orm_common.utils import utils
from orm.common.orm_common.utils.cross_api_utils import (
    get_regions_of_group,
    set_utils_conf)
from orm.services.customer_manager.cms_rest.data.data_manager import \
    DataManager
from orm.services.customer_manager.cms_rest.logger import get_logger
from orm.common.orm_common.utils.error_base import (
    ConflictError,
    ErrorStatus,
    InputValueError,
    NotAllowedError,
    NotFoundError)
from orm.services.customer_manager.cms_rest.model.GroupModels import (
    GroupResultWrapper,
    GroupSummary,
    GroupSummaryResponse,
    RegionResultWrapper,
    RegionUserResultWrapper,
    RoleResult,
    RoleResultWrapper,
    UserResultWrapper,
    RegionResultWrapper)

from orm.services.customer_manager.cms_rest.rds_proxy import RdsProxy

LOG = get_logger(__name__)


class GroupLogic(object):

    def build_full_group(self, group, uuid, datamanager):
        if any(char in ":" for char in group.name):
            raise InputValueError("Group Name does not allow colon(:).")

        if group.name.strip() == '':
            raise InputValueError("Group Name can not be blank.")

        try:
            sql_group = datamanager.add_group(group, uuid)

        except oslo_db.exception.DBDuplicateEntry as exception:
            raise ConflictError(
                "Group name '{}' already exists".format(group.name))

        except Exception as exp:
            LOG.log_exception("CustomerLogic - Failed to CreateCustomer", exp)
            datamanager.rollback()
            raise

        datamanager.add_group_region(sql_group.uuid, -1)

        # add any region along with the users assigned to it to the group
        self.add_regions_to_db(group.regions, sql_group.uuid, datamanager)

        # add group default users
        self.add_default_user_db(datamanager, group.users, [], sql_group.uuid)
        # add default user(s) to all regions where group is assigned to
        self.add_default_users_to_regions_db(
            datamanager, sql_group, group.users)

        self.add_roles_to_db(group.roles, sql_group.uuid, datamanager)

        return sql_group

    def add_region_users_to_db(self, datamanager, group_uuid, region_id):
        # create region user record(s) for every default user(s) for the group
        # found in groups_user table

        group_record = datamanager.get_record('group')
        group = group_record.read_group_by_uuid(group_uuid)

        defaultRegion = group.get_default_region()

        # create region users from existing default users
        existing_default_users =\
            defaultRegion.group_region_users if defaultRegion else []

        for user in existing_default_users:
            datamanager.add_groups_user(group_uuid, user.user.id,
                                        region_id, user.domain_name)

    def add_regions_to_db(self, regions, sql_group_id,
                          datamanager, default_users=[]):
        for region in regions:

            sql_region = datamanager.add_region(region)
            try:
                datamanager.add_group_region(sql_group_id, sql_region.id)
            except Exception as ex:
                if hasattr(ex, 'orig') and ex.orig[0] == 1062:
                    raise ConflictError(
                        'Error, duplicate entry, region '
                        + region.name
                        + ' already associated with group')
                raise ex

            self.add_user_db(datamanager, region.users,
                             default_users, sql_group_id, sql_region.id)

    def add_default_user_db(self, datamanager, default_users_requested,
                            existing_default_users, group_uuid):
        default_region_users = []
        for user_info in default_users_requested:
            domain_value = user_info.domain
            for username in user_info.id:
                default_user_exists = []
                if existing_default_users:
                    # check if there is user/user_domain match
                    # in existing_default_users list

                    default_user_exists =\
                        [username for exist_user in existing_default_users
                            if exist_user.user.name == username
                            and exist_user.domain_name == domain_value]

                if not default_user_exists:
                    # add user to cms_user table and group_users
                    sql_user = datamanager.add_user(username)
                    sql_groups_user = \
                        datamanager.add_groups_user(group_uuid, sql_user.id,
                                                    -1, domain_value)
                    default_region_users.append(sql_groups_user)

    def add_user_db(self, datamanager, region_users_requested,
                    all_existing_users, group_uuid, region_id):

        for user_info in region_users_requested:
            domain_value = user_info.domain
            for username in user_info.id:
                region_user_exists = []
                if all_existing_users:
                    # check if there is user/user_domain match
                    # in existing_default_users list
                    region_user_exists =\
                        [username for exist_user in all_existing_users
                            if exist_user.user.name == username
                            and exist_user.domain_name == domain_value]

                if not region_user_exists:
                    # add user to cms_user table and group_users
                    sql_user = datamanager.add_user(username)
                    sql_groups_user = \
                        datamanager.add_groups_user(group_uuid, sql_user.id,
                                                    region_id, domain_value)

    def add_roles_to_db(self, role_assignments, group_uuid, datamanager,
                        region=None):
        if not role_assignments:
            return

        [assignment.validate_model() for assignment in role_assignments]
        region_record = datamanager.get_record('groups_region')

        # If region is not specified, then get all the regions already
        # associated with the group for role assginement; otherwise,
        # just assign roles for the passed in region only.
        if region is None:
            groups_regions = region_record.get_regions_for_group(group_uuid)
        else:
            groups_regions = [region_record.get_region_by_keys(group_uuid,
                                                               region)]
        for role_assignment in role_assignments:
            for role in role_assignment.roles:
                sql_role = datamanager.add_role(role)
                role_id = sql_role.id
                for group_region in groups_regions:
                    region_id = group_region.region_id

                    if role_assignment.domain:
                        datamanager.add_groups_role_on_domain(
                            group_uuid,
                            role_id,
                            region_id,
                            role_assignment.domain)
                    elif role_assignment.customer:
                        customer_id = datamanager.get_customer_id_by_uuid(
                            role_assignment.customer)
                        datamanager.add_groups_role_on_customer(
                            group_uuid,
                            role_id,
                            region_id,
                            customer_id)
        datamanager.flush()

    def assign_roles(self,
                     group_uuid,
                     role_assignments,
                     transaction_id,
                     region=None):

        datamanager = DataManager()
        try:
            self.add_roles_to_db(
                role_assignments, group_uuid, datamanager, region)

            group_record = datamanager.get_record('group')
            group = group_record.read_group_by_uuid(group_uuid)
            group_dict = group.get_proxy_dict()

            if len(group_dict["regions"]) > 0:
                RdsProxy.send_group_dict(group_dict, transaction_id, "PUT")

            roles = [{'roles': role_assignment.roles,
                      'domain': role_assignment.domain,
                      'customer': role_assignment.customer}
                     for role_assignment in role_assignments]
            role_result_wrapper = build_response(group_uuid,
                                                 transaction_id,
                                                 'role_assignment',
                                                 roles=roles)
            datamanager.commit()
            return role_result_wrapper
        except Exception as exp:
            LOG.log_exception("GroupLogic - Failed to Assign Role(s)", exp)
            datamanager.rollback()
            raise

    def add_default_users_to_regions_db(self, datamanager, sql_group,
                                        default_users, regions=[]):
        ''' assign default users to all regions where the group is associated with
        '''
        if not regions:
            regions = sql_group.get_group_regions()

        for region in regions:
            self.add_user_db(datamanager, default_users, [],
                             sql_group.uuid, region.region_id)

    def add_group_default_users(self, group_uuid, users, transaction_id,
                                p_datamanager=None):

        LOG.info("Add default users: group: {} user: {} ".format(
                 group_uuid, users))
        datamanager = None
        try:
            # p_datamanager is passed by replace_default_users
            if p_datamanager is None:
                datamanager = DataManager()
                datamanager.begin_transaction()
            # else:
            #     datamanager = p_datamanager

            group_id = datamanager.get_group_by_uuid_or_name(group_uuid)

            if group_id is None:
                raise NotFoundError("group {} does not exist".format(
                                    group_uuid))

            group_record = datamanager.get_record('group')
            group = group_record.read_group_by_uuid(group_uuid)

            defaultRegion = group.get_default_region()
            existing_default_users =\
                defaultRegion.group_region_users if defaultRegion else []

            default_users = []
            for default_user in existing_default_users:
                if default_user.user not in default_users:
                    default_users.append(default_user)

            self.add_default_user_db(datamanager, users,
                                     existing_default_users, group_uuid)

            # add default user(s) to all regions where group is assigned to
            self.add_default_users_to_regions_db(datamanager, group, users)

            timestamp = utils.get_time_human()
            datamanager.flush()

            group_dict = group.get_proxy_dict()
            if len(group.group_regions) > 1:
                # RdsProxy.send_group(group, transaction_id, "PUT")
                RdsProxy.send_group_dict(group_dict, transaction_id, "PUT")

            if p_datamanager is None:
                users_result = [{'id': user.id,
                                 'domain': user.domain} for user in users]
                user_result_wrapper = build_response(group_uuid,
                                                     transaction_id,
                                                     'add_group_default_users',
                                                     users=users_result)

                datamanager.commit()
                return user_result_wrapper

        except Exception as exception:
            datamanager.rollback()
            if 'Duplicate' in str(exception):
                raise ConflictError(str(exception))
            LOG.log_exception("Failed to add_group_default_users", exception)
            raise

    #  this function is used to assign users to a specific region
    def add_group_region_users(self, group_uuid, region_id,
                               region_users_requested,
                               transaction_id, p_datamanager=None):
        LOG.info("Add user under group region: group: {} "
                 "region: {}".format(group_uuid, region_id))
        datamanager = None

        try:
            # p_datamanager is passed by replace_default_users
            if p_datamanager is None:
                datamanager = DataManager()
                datamanager.begin_transaction()
            # else:
            #     datamanager = p_datamanager

            group_id = datamanager.get_group_by_uuid_or_name(group_uuid)
            region_id = datamanager.get_region_id_by_name(region_id)

            if group_id is None:
                raise NotFoundError("group {} does not exist".format(
                                    group_uuid))

            if region_id is None:
                raise NotFoundError("region {} does not exist".format(
                                    region_uuid))

            group_record = datamanager.get_record('group')
            group = group_record.read_group_by_uuid(group_uuid)
            groupRegion = group.get_region(region_id)

            # get all users already assigned to the group region
            current_region_users = \
                groupRegion.group_region_users if groupRegion else []

            # build the existing_users_list from current region users result
            region_users_list = []
            for rgn_user in current_region_users:
                if rgn_user.user not in region_users_list:
                    region_users_list.append(rgn_user)

            self.add_user_db(datamanager, region_users_requested,
                             region_users_list, group_uuid, region_id)
            timestamp = utils.get_time_human()
            datamanager.flush()

            group_dict = group.get_proxy_dict()

            if len(group.group_regions) > 1:
                RdsProxy.send_group_dict(group_dict, transaction_id, "PUT")

            if p_datamanager is None:
                datamanager.commit()

            users_result =\
                [{'id': user.id,
                  'domain': user.domain} for user in region_users_requested]
            region_user_result_wrapper =\
                build_response(group_uuid, transaction_id,
                               'add_group_region_users',
                               users=users_result)

            return region_user_result_wrapper

        except Exception as exception:
            datamanager.rollback()
            if 'Duplicate' in str(exception):
                raise ConflictError(str(exception))
            LOG.log_exception("Failed to add_group_region_users", exception)
            raise

    def delete_group_default_user(self, group_uuid, user, domain,
                                  transaction_id):
        LOG.info("Delete default user: group: {0} user: {1} "
                 " user domain: {2}".format(group_uuid, user, domain))

        datamanager = DataManager()

        try:
            group = datamanager.get_group_by_uuid_or_name(group_uuid)
            if group is None:
                raise NotFoundError("group {} does not exist".format(
                                    group_uuid))

            user_record = datamanager.get_record('groups_user')
            result = user_record.remove_user_from_group(group_uuid, -1,
                                                        domain, user)

            if result.rowcount == 0:
                raise NotFoundError("user {}@{} domain".format(user, domain))
            datamanager.flush()

            group_record = datamanager.get_record('group')
            group = group_record.read_group_by_uuid(group_uuid)
            group_dict = group.get_proxy_dict()

            if len(group.group_regions) > 1:
                RdsProxy.send_group_dict(group_dict, transaction_id, "PUT")

            datamanager.commit()

            LOG.info("User {0} from region {1} in group {2} deleted".
                     format(user, 'DEFAULT', group_uuid))

        except NotFoundError as e:
            datamanager.rollback()
            LOG.log_exception("Failed to delete default user, user not found",
                              str(e))
            raise NotFoundError("Failed to delete default user,"
                                " default %s not found" % str(e))
            raise

        except Exception as exp:
            datamanager.rollback()
            raise exp

    def delete_group_region_user(self, group_uuid, region_id, user,
                                 user_domain, transaction_id):
        LOG.info("Delete user: group: {0} region: {1} user: {2} user "
                 "domain: {3}".format(group_uuid, region_id, user,
                                      user_domain))
        datamanager = DataManager()

        try:
            group = datamanager.get_group_by_uuid_or_name(group_uuid)
            if group is None:
                raise NotFoundError("group {} does not exist".format(
                                    group_uuid))
            user_record = datamanager.get_record('groups_user')
            result = user_record.remove_user_from_group(group_uuid, region_id,
                                                        user_domain, user)

            if result.rowcount == 0:
                '''result.rowcount = 0 indicates that the region user
                requested for deletion is identified as default user for the
                group.  Since default user supersedes region user, use
                'delete_group_default_user' command instead to delete the user.

                '''
                message = "Cannot use 'delete_group_region_user' as user " \
                          "%s@%s domain is a default user for "\
                          "group %s.  Use 'delete_group_default_user' "\
                          "instead." % (user, user_domain, group_uuid)
                raise ErrorStatus(message)

            group_record = datamanager.get_record('group')
            group = group_record.read_group_by_uuid(group_uuid)
            group_dict = group.get_proxy_dict()
            RdsProxy.send_group_dict(group_dict, transaction_id, "PUT")

            datamanager.commit()

            LOG.info("User {0} with user domain {1} from region {2} "
                     "in group {3} deleted".format(user, user_domain,
                                                   region_id, group_uuid))

        except NotFoundError as e:
            datamanager.rollback()
            LOG.log_exception("Failed to delete region user,"
                              " user not found", str(e))
            raise NotFoundError("Failed to delete region user,"
                                " region %s not found" % str(e))
        except Exception as exception:
            datamanager.rollback()
            LOG.log_exception("Failed to delete region user", exception)
            raise exception

    def unassign_roles(self,
                       group_uuid,
                       role_name,
                       assignment_type,
                       assignment_value,
                       transaction_id):

        datamanager = DataManager()
        try:
            group_record = datamanager.get_record('group')
            region_record = datamanager.get_record('groups_region')
            groups_regions = region_record.get_regions_for_group(group_uuid)
            groups_role = datamanager.get_record('groups_role')
            sql_group = datamanager.get_group_by_uuid_or_name(group_uuid)

            if assignment_type != "customer" and assignment_type != "domain":
                raise InputValueError("Role unassignment type must either be "
                                      "domain or project.")

            if sql_group is None:
                raise NotFoundError(
                    "group with id {} does not exist".format(group_uuid))

            role_id = datamanager.get_role_id_by_name(role_name)
            domain = datamanager.get_record('groups_domain_role')
            customer = datamanager.get_record('groups_customer_role')

            for group_region in groups_regions:
                region_id = group_region.region_id

                if assignment_type == "domain":
                    domain.remove_domain_role_from_group(
                        group_uuid, region_id, assignment_value, role_id)

                elif assignment_type == "customer":
                    customer_id = datamanager.get_customer_id_by_uuid(
                        assignment_value)
                    if customer_id is None:
                        raise NotFoundError(
                            "customer uuid [{}] does not exist".format(
                                assignment_value))

                    customer.remove_customer_role_from_group(
                        group_uuid, region_id, customer_id, role_id)

                if (not customer.check_groups_customer_role_exist(
                        role_id, group_uuid)
                    and not domain.check_groups_domain_role_exist(
                        role_id, group_uuid)):
                    groups_role.remove_role_from_group(group_uuid, role_id)

            datamanager.flush()
            group = group_record.read_group_by_uuid(group_uuid)
            group_dict = group.get_proxy_dict()

            if len(group_dict["regions"]) > 0:
                RdsProxy.send_group_dict(group_dict, transaction_id, "PUT")

            datamanager.commit()
            LOG.info("Role unassgined - type: {} value: {} group: {} "
                     "role {} ".format(assignment_type, assignment_value,
                                       group_uuid, role_name))
        except Exception as exp:
            datamanager.rollback()
            raise

    def create_group(self, group, uuid, transaction_id):
        datamanager = DataManager()
        try:
            group.handle_region_group()
            sql_group = self.build_full_group(group, uuid, datamanager)
            group_result_wrapper = build_response(uuid,
                                                  transaction_id,
                                                  'create_group')
            if sql_group.group_regions and len(sql_group.group_regions) > 1:
                group_dict = sql_group.get_proxy_dict()
                for region in group_dict["regions"]:
                    region["action"] = "create"

                datamanager.flush()
                RdsProxy.send_group_dict(group_dict, transaction_id, "POST")
            else:
                LOG.debug(
                    "Group with no regions - wasn't send to RDS Proxy "
                    + str(group))

            datamanager.commit()

        except Exception as exp:
            LOG.log_exception("GroupLogic - Failed to CreateGroup", exp)
            datamanager.rollback()
            raise

        return group_result_wrapper

    def resolve_regions_actions(self, old_regions_dict, new_regions_dict):
        for region in new_regions_dict:
            old_region = next(
                (r for r in old_regions_dict if r["name"] == region["name"]),
                None)
            if old_region:
                region["action"] = "modify"
            else:
                region["action"] = "create"

        for region in old_regions_dict:
            new_region = next(
                (r for r in new_regions_dict if r["name"] == region["name"]),
                None)
            if not new_region:
                region["action"] = "delete"
                new_regions_dict.append(region)

        return new_regions_dict

    def update_group(self, group, group_uuid, transaction_id):
        datamanager = DataManager()
        try:
            group.validate_model('update')
            group_record = datamanager.get_record('group')
            group_id = group_record.get_group_id_from_uuid(
                group_uuid)

            sql_group = group_record.read_group_by_uuid(group_uuid)
            if not sql_group:
                raise NotFoundError(
                    'group {0} was not found'.format(group_uuid))

            # old_group_dict = sql_group.get_proxy_dict()
            group_record.delete_by_primary_key(group_id)
            datamanager.flush()

            sql_group = self.build_full_group(group,
                                              group_uuid,
                                              datamanager)

            # new_group_dict = sql_group.get_proxy_dict()
            # new_group_dict["regions"] = self.resolve_regions_actions(
            #     old_group_dict["regions"],
            #     new_group_dict["regions"])

            datamanager.flush()
            # if not len(new_group_dict['regions']) == 0:
            #    RdsProxy.send_group_dict(
            #        new_group_dict, transaction_id, "PUT")

            group_result_wrapper = build_response(group_uuid,
                                                  transaction_id,
                                                  'update_group')
            datamanager.commit()
            return group_result_wrapper

        except Exception as exp:
            LOG.log_exception("GroupLogic - Failed to UpdateGroup", exp)
            datamanager.rollback()
            raise

    def add_regions(self, group_id, regions, transaction_id):
        datamanager = DataManager()
        group_region = datamanager.get_record('groups_region')
        try:
            sql_group = datamanager.get_group_by_uuid_or_name(group_id)
            if sql_group is None:
                raise NotFoundError(
                    "group with id {} does not exist".format(group_id))
            defaultRegion = sql_group.get_default_region()
            default_users =\
                defaultRegion.group_region_users if defaultRegion else []
            self.add_regions_to_db(regions, group_id, datamanager,
                                   default_users)

            # create additional region users from default group users
            for region in regions:
                sql_region = datamanager.add_region(region)
                self.add_region_users_to_db(datamanager, group_id,
                                            sql_region.id)

            datamanager.commit()
            datamanager.session.expire(sql_group)

            sql_group = datamanager.get_group_by_uuid_or_name(group_id)
            group_dict = sql_group.get_proxy_dict()

            for region in group_dict["regions"]:
                new_region = next((r for r in regions
                                  if r.name == region["name"]), None)
                if new_region:
                    region["action"] = "create"
                else:
                    region["action"] = "modify"

            timestamp = utils.get_time_human()
            RdsProxy.send_group_dict(group_dict, transaction_id, "POST")
            base_link = '{0}{1}/'.format(conf.server.host_ip,
                                         request.path)
            result_regions = [{'id': region.name, 'added': timestamp,
                               'links': {'self': base_link + region.name}} for
                              region in regions]
            region_result_wrapper = RegionResultWrapper(
                transaction_id=transaction_id, regions=result_regions)
            return region_result_wrapper
        except Exception as exp:
            datamanager.rollback()
            raise
        finally:
            datamanager.close()

    def delete_region(self, group_id, region_id, transaction_id, force_delete):
        datamanager = DataManager()
        try:
            group_region = datamanager.get_record('groups_region')
            sql_group = datamanager.get_group_by_uuid_or_name(group_id)
            if sql_group is None:
                raise NotFoundError(
                    "group with id {} does not exist".format(group_id))

            group_dict = sql_group.get_proxy_dict()
            group_region.delete_region_for_group(group_id, region_id)
            datamanager.flush()

            region = next((r.region for r in sql_group.group_regions
                          if r.region.name == region_id), None)
            if region:
                if region.type == 'group':
                    set_utils_conf(conf)
                    regions = get_regions_of_group(region.name)
                else:
                    regions = [region_id]
            for region in group_dict['regions']:
                if region['name'] in regions:
                    region['action'] = 'delete'

            RdsProxy.send_group_dict(group_dict, transaction_id, "PUT")
            if force_delete:
                datamanager.commit()
            else:
                datamanager.rollback()

        except Exception as exp:
            datamanager.rollback()
            raise

        finally:
            datamanager.close()

    def get_group(self, group):
        datamanager = DataManager()
        sql_group = datamanager.get_group_by_uuid_or_name(group)

        if not sql_group:
            raise NotFoundError('group: {0} not found'.format(group))
        ret_group = sql_group.to_wsme()

        if sql_group.get_group_regions():
            resp = requests.get(conf.api.rds_server.base
                                + conf.api.rds_server.status
                                + sql_group.uuid, verify=conf.verify).json()
            for item in ret_group.regions:
                for status in resp['regions']:
                    if status['region'] == item.name:
                        item.status = status['status']
                        if status['error_msg']:
                            item.error_message = status['error_msg']
            ret_group.status = resp['status']
        else:
            ret_group.status = 'no regions'

        return ret_group

    def get_group_list_by_criteria(self, region, user, starts_with, contains,
                                   start=0, limit=0):
        datamanager = DataManager()
        group_record = datamanager.get_record('group')
        sql_groups = group_record.get_groups_by_criteria(
            region=region,
            user=user,
            starts_with=starts_with,
            contains=contains,
            start=start,
            limit=limit)
        response = GroupSummaryResponse()
        if sql_groups:
            uuids = [sql_group.uuid for sql_group in sql_groups
                     if sql_group and sql_group.uuid]

            sql_in = ', '.join(list(["'%s'" % arg for arg in uuids]))
            resource_status = group_record.get_groups_status_by_uuids(sql_in)

            for sql_group in sql_groups:
                groups = GroupSummary.from_db_model(sql_group)

                if sql_group.uuid:
                    # rds_region list contains tuples - each containing the
                    # region associated  with the customer along with the
                    # region status
                    rds_region = resource_status.get(sql_group.uuid)
                    if rds_region and groups.regions:
                        # set customer.status to 'error' if any of the regions
                        # has an 'Error' status' else, if any region status
                        # shows 'Submitted' then set customer status to
                        # 'Pending'; otherwise customer status is 'Success'
                        error_status = [item for item in rds_region
                                        if item[1] == 'Error']
                        submitted_status = [item for item in rds_region
                                            if item[1] == 'Submitted']
                        success_status = [item for item in rds_region
                                          if item[1] == 'Success']

                        if len(error_status) > 0:
                            groups.status = 'Error'
                        elif len(submitted_status) > 0:
                            groups.status = 'Pending'
                        elif len(success_status) > 0:
                            groups.status = 'Success'
                    else:
                        groups.status = 'no regions'
                response.groups.append(groups)
        return response

    def get_group_roles_by_criteria(
            self, group_uuid, region_name, customer_uuid, domain_name):

        if region_name is None:
            raise NotAllowedError("region must be specified in request "
                                  "uri query.")
        if customer_uuid is not None and domain_name is not None:
            raise NotAllowedError("customer and domain cannot be used at "
                                  "the same time for query in request uri.")

        role_result = []
        roles = []
        datamanager = DataManager()
        # filter by region
        if customer_uuid is None and domain_name is None:
            record = datamanager.get_record('groups_customer_role')
            sql_customers_roles = record.get_customer_roles_by_region(
                group_uuid, region_name)

            record = datamanager.get_record('groups_domain_role')
            sql_domains_roles = record.get_domain_roles_by_region(
                group_uuid, region_name)

            unique_customer = {}
            for customer in sql_customers_roles:
                if customer.customer.uuid in unique_customer:
                    unique_customer[customer.customer.uuid].append(
                        customer.groups_role.role.name)
                else:
                    unique_customer[customer.customer.uuid] = [
                        customer.groups_role.role.name]

            for customer, role_list in list(unique_customer.items()):
                role_result.append(
                    RoleResult(roles=role_list, customer=customer))

            unique_domain = {}
            for domain in sql_domains_roles:
                if domain.domain_name in unique_domain:
                    unique_domain[domain.domain_name].append(
                        domain.groups_role.role.name)
                else:
                    unique_domain[domain.domain_name] = [
                        domain.groups_role.role.name]

            for domain, role_list in list(unique_domain.items()):
                role_result.append(RoleResult(roles=role_list, domain=domain))

            return role_result

        # filter by customer
        elif customer_uuid is not None:
            customer_id = datamanager.get_customer_id_by_uuid(customer_uuid)
            record = datamanager.get_record('groups_customer_role')
            sql_roles = record.get_customer_roles_by_criteria(
                group_uuid, region_name, customer_id)

        # filter by domain
        else:
            record = datamanager.get_record('groups_domain_role')
            sql_roles = record.get_domain_roles_by_criteria(
                group_uuid, region_name, domain_name)

        if sql_roles:
            roles = [sql_role.groups_role.role.name for sql_role in sql_roles
                     if sql_role and sql_role.groups_role.role.name]

        return [RoleResult(roles=roles)]

    def delete_group_by_uuid(self, group_id):
        datamanager = DataManager()

        try:
            datamanager.begin_transaction()
            group_record = datamanager.get_record('group')

            sql_group = group_record.read_group_by_uuid(group_id)
            if sql_group is None:
                raise NotFoundError(
                    "Group '{0}' not found".format(group_id))

            regions = sql_group.get_group_regions()
            if len(regions) > 0:
                # Do not delete a group that still has region(s)
                raise NotAllowedError(
                    " Cannot delete a group that has region(s)."
                    " Please delete the region(s) first and then"
                    " delete the group.")
            else:
                expected_status = 'Success'
                invalid_status = 'N/A'
                # Get status from RDS
                resp = RdsProxy.get_status(sql_group.uuid)
                if resp.status_code == 200:
                    status_resp = resp.json()
                    if 'status' in list(status_resp.keys()):
                        LOG.debug('RDS returned status: {}'.format(
                                  status_resp['status']))
                        status = status_resp['status']
                    else:
                        # Invalid response from RDS
                        LOG.error('Response from RDS did not contain status')
                        status = invalid_status
                elif resp.status_code == 404:
                    # Group not found in RDS, that means it never has any
                    # region(s). So it is OK to delete it.
                    LOG.debug(
                        'Resource not found in RDS, so it is OK to delete')
                    status = expected_status
                else:
                    # Invalid status code from RDS
                    log_message = 'Invalid response code from RDS: {}'.format(
                        resp.status_code)
                    log_message = log_message.replace('\n', '_').replace('\r',
                                                                         '_')
                    LOG.warning(log_message)
                    status = invalid_status

                if status == invalid_status:
                    raise ErrorStatus("Could not get group status")
                elif status != expected_status:
                    raise ErrorStatus(
                        "The group has not been deleted "
                        "successfully from all of its regions "
                        "(either the deletion failed on one of the "
                        "regions or it is still in progress)")

            # OK to delete
            group_record.delete_group_by_uuid(group_id)
            datamanager.flush()
            datamanager.commit()
        except Exception as exp:
            LOG.log_exception("GroupLogic - Failed to delete group", exp)
            datamanager.rollback()
            raise


def build_response(group_uuid, transaction_id, context, roles=[], users=[]):
    """this function generate th group action response JSON
    :param group_uuid:
    :param transaction_id:
    :param context:
    :param roles:
    :param users:
    :return:
    """
    timestamp = utils.get_time_human()
    # The link should point to the group itself (/v1/orm/groups/{id})
    link_elements = request.url.split('/')
    base_link = '/'.join(link_elements)
    if context == 'create_group' or context == 'update_group':
        if context == 'create_group':
            base_link = base_link + group_uuid

        return GroupResultWrapper(transaction_id=transaction_id,
                                  id=group_uuid,
                                  updated=None,
                                  created=timestamp,
                                  links={'self': base_link})

    elif context == 'role_assignment':
        return RoleResultWrapper(transaction_id=transaction_id,
                                 roles=roles,
                                 links={'self': base_link},
                                 created=timestamp)

    elif context == 'add_group_default_users':
        return UserResultWrapper(transaction_id=transaction_id,
                                 users=users,
                                 links={'self': base_link},
                                 created=timestamp)
    elif context == 'add_group_region_users':
        return RegionUserResultWrapper(transaction_id=transaction_id,
                                       users=users,
                                       links={'self': base_link},
                                       created=timestamp)

    else:
        return None
