"""ORM controller module."""
from orm.services.region_manager.rms.controllers.v2.orm.resources import \
    groups, regions, uuid


class OrmController(object):
    """ORM controller class."""

    regions = regions.RegionsController()
    groups = groups.GroupsController()
    uuid = uuid.UUIDController()
