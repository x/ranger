import json
import logging

from orm.common.orm_common.utils import api_error_utils as err_utils
from orm.services.region_manager.rms.utils import authentication

from pecan import conf
from pecan import request
from pecan import rest
import requests
import wsme
from wsme import types as wtypes
from wsmeext.pecan import wsexpose

logger = logging.getLogger(__name__)


class UUIDData(wtypes.DynamicBase):
    id = wsme.wsattr(wtypes.text, mandatory=True)
    uuid = wsme.wsattr(wtypes.text, mandatory=True)
    uuid_type = wsme.wsattr(wtypes.text, mandatory=True)

    def __init__(self, id=None, uuid=None, uuid_type=None):
        self.id = id
        self.uuid = uuid
        self.uuid_type = uuid_type


class UUIDController(rest.RestController):

    @wsexpose(UUIDData, str, status_code=200, rest_content_types='json')
    def get_one(self, uuid):
        logger.info("UUIDController - Getting UUID " + uuid)
        authentication.authorize(request, 'uuid:get_one')

        url = "{}{}/{}".format(conf.api.uuid_server.base,
                               conf.api.uuid_server.uuids,
                               uuid)
        try:
            resp = requests.get(url)
        except Exception as ex:
            logger.exception(
                "Failed to get uuid : {}".format(str(ex)))
            raise err_utils.get_error(request.transaction_id,
                                      status_code=500,
                                      message=str(ex))

        if resp.status_code != 200:
            message = "Failed to uuid {}: {}".format(uuid, resp.text)
            raise err_utils.get_error(request.transaction_id,
                                      status_code=resp.status_code,
                                      message=message)

        uuid_data = json.loads(resp.content)
        return UUIDData(id=str(uuid_data['id']),
                        uuid=uuid_data['uuid'],
                        uuid_type=uuid_data['uuid_type'])

    @wsexpose(None, str, rest_content_types='json', status_code=204)
    def delete(self, uuid):
        logger.info("UUIDController - Deleting UUID " + uuid)
        authentication.authorize(request, 'uuid:delete')

        headers = {}
        headers['X-Auth-Region'] = request.headers[
            'X-Auth-Region'] if 'X-Auth-Region' in \
            request.headers else ''
        headers['X-Auth-Token'] = request.headers[
            'X-Auth-Token'] if 'X-Auth-Token' in \
            request.headers else ''

        url = "{}{}/{}".format(conf.api.uuid_server.base,
                               conf.api.uuid_server.uuids,
                               uuid)
        try:
            resp = requests.delete(url, headers=headers)
        except Exception as ex:
            logger.exception(
                "Failed to delete uuid : {}".format(str(ex)))
            raise err_utils.get_error(request.transaction_id,
                                      status_code=500,
                                      message=str(ex))

        if resp.status_code != 200:
            message = "Failed to delete uuid {}: {}".format(uuid, resp.text)
            raise err_utils.get_error(request.transaction_id,
                                      status_code=resp.status_code,
                                      message=message)
        return
