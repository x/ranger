import logging

from orm.common.orm_common.utils.error_base import SQLDBError
from orm.services.region_manager.rms.storage.my_sql.data_manager import DataManager

from pecan import conf


LOG = logging.getLogger(__name__)


def get_data_manager():
    try:
        dm = DataManager(url=conf.database.url,
                         max_retries=conf.database.max_retries,
                         retries_interval=conf.database.retries_interval)
        return dm
    except Exception:
        nagios_message = "CRITICAL|CONDB001 - Could not establish " \
                         "database connection"
        LOG.error(nagios_message)
        raise SQLDBError("Could not establish database connection")
