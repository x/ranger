import json
import logging

from orm.common.client.keystone.keystone_utils import tokens
from orm.common.orm_common.policy import policy
from orm.services.region_manager.rms.services import services as RegionService

from pecan import conf

logger = logging.getLogger(__name__)


def get_keystone_ep(auth_region):
    result = RegionService.get_region_by_id_or_name(auth_region)
    for ep in result.endpoints:
        if ep.type == 'identity':
            return ep.publicurl

    # Keystone EP not found
    return None


def authorize(request, action, skip_auth=False):
    if not _is_authorization_enabled(conf) or skip_auth:
        return

    use_payload_url =\
        action == 'region:create'
    keystone_ep = None
    try:
        if use_payload_url:
            keystone_ep = _get_request_keystone_ep(request)
        if not keystone_ep:
            auth_region = request.headers.get('X-Auth-Region')
            keystone_ep = get_keystone_ep(auth_region)
    except Exception:
        # Failed to find Keystone EP - we'll set it to None instead of failing
        # because the rule might be to let everyone pass
        keystone_ep = None
    policy.authorize(action, request, conf, keystone_ep=keystone_ep)


def _is_authorization_enabled(app_conf):
    return app_conf.authentication.enabled


def get_token_conf(app_conf):
    mech_id = app_conf.authentication.mech_id
    mech_password = app_conf.authentication.mech_pass
    # RMS URL is not necessary since this service is RMS
    rms_url = ''
    tenant_name = app_conf.authentication.tenant_name
    keystone_version = app_conf.authentication.keystone_version
    user_domain_name = app_conf.authentication.user_domain_name
    project_domain_name = app_conf.authentication.project_domain_name
    conf = tokens.TokenConf(mech_id, mech_password, rms_url, tenant_name,
                            keystone_version, user_domain_name,
                            project_domain_name)

    return conf


def _get_request_keystone_ep(request):
    keystone_ep = None
    try:
        request_body = request.body if request.body else {}
        endpoints = json.loads(request_body).get('endpoints')
        endpoint_url = [endpoint.get('publicURL')
                        for endpoint in endpoints
                        if endpoint.get('type') == 'identity']
        keystone_ep = endpoint_url[0] if endpoint_url else None
    except RangerException:
        keystone_ep = None

    return keystone_ep
