from orm.common.orm_common.hooks.transaction_id_hook import TransactionIdHook
from orm.common.orm_common.utils import utils

from pecan import abort


class TransIdHook(TransactionIdHook):

    def before(self, state):
        try:
            controller = str(state.controller)
            if 'RootController._default' in controller:
                return
            else:
                transaction_id = utils.make_transid()
        except Exception as exc:
            abort(500, headers={'faultstring': str(exc)})

        tracking_id = state.request.headers['X-RANGER-Tracking-Id'] \
            if 'X-RANGER-Tracking-Id' in state.request.headers else transaction_id
        setattr(state.request, 'transaction_id', transaction_id)
        setattr(state.request, 'tracking_id', tracking_id)
