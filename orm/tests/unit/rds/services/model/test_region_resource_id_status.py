import unittest

from orm.common.orm_common.model import models
from orm.services.resource_distributor.rds.services.model import region_resource_id_status


class TestModel(unittest.TestCase):
    def test_model_as_dict(self):
        model = models.ResourceStatusModel(
            1, 2, 3, 4, 5, 6, 7, 8, 'create')
        expected_dict = {
            'timestamp': 1,
            'region': 2,
            'status': 3,
            'ord_transaction_id': 4,
            'resource_id': 5,
            'ord_notifier_id': 6,
            'error_msg': 7,
            'error_code': 8,
            'operation': 'create',
            'resource_extra_metadata': None
        }

        test_dict = model.as_dict()
        self.assertEqual(test_dict, expected_dict)


class TestResourceTemplateModel(unittest.TestCase):
    def test_model_as_dict(self):
        model = region_resource_id_status.ResourceTemplateModel(1, 2, 3, 4, 5)

        expected_template_dict = {
            'resource_id': 1,
            'resource_name': 2,
            'region': 3,
            'template_version': 4,
            'template_data': 5
        }

        test_dict = model.as_dict()
        self.assertEqual(test_dict, expected_template_dict)


class TestStatusModel(unittest.TestCase):
    def test_get_aggregated_status_error(self):
        model = models.ResourceStatusModel(
            1, 2, 'Error', 4, 5, 6, 7, 8, 'create')
        status_model = models.StatusModel([model])
        self.assertEqual(status_model.status, 'Error')

    def test_get_aggregated_status_pending(self):
        model = models.ResourceStatusModel(
            1, 2, 'Submitted', 4, 5, 6, 7, 8, 'create')
        status_model = models.StatusModel([model])
        self.assertEqual(status_model.status, 'Pending')

    def test_get_aggregated_status_success(self):
        model = models.ResourceStatusModel(
            1, 2, 'Success', 4, 5, 6, 7, 8, 'create')
        status_model = models.StatusModel([model])
        self.assertEqual(status_model.status, 'Success')
