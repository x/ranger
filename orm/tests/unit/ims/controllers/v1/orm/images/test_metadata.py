from orm.common.orm_common.utils.error_base import ClientSideError, NotFoundError
from orm.services.image_manager.ims.controllers.v1.orm.images import metadata
from orm.tests.unit.ims import FunctionalTest

import mock

metadata_input = {
    "metadata": {
        "checksum": "1",
        "virtual_size": "@",
        "size": "3"
    }
}


class TestMetaDataController(FunctionalTest):
    """metadata controller(api) unittests."""

    @staticmethod
    def get_error(transaction_id, status_code, error_details=None,
                  message=None):
        return ClientSideError(message or error_details, status_code)

    def setUp(self):
        FunctionalTest.setUp(self)

    def tearDown(self):
        FunctionalTest.tearDown(self)

    @mock.patch.object(metadata, 'di')
    def test_post_metadata_success(self, mock_di):
        mock_di.resolver.unpack.return_value = get_mocks()
        response = self.app.post_json(
            '/v1/orm/images/image_id/regions/region_name/metadata',
            metadata_input)
        self.assertEqual(200, response.status_code)

    @mock.patch.object(metadata, 'err_utils')
    @mock.patch.object(metadata, 'di')
    def test_post_metadata_not_found(self, mock_di, mock_error_utils):
        mock_error_utils.get_error = self.get_error
        mock_di.resolver.unpack.return_value = get_mocks(error=404)
        response = self.app.post_json(
            '/v1/orm/images/image_id/regions/region_name/metadata',
            metadata_input, expect_errors=True)
        self.assertEqual(404, response.status_code)
        self.assertEqual(response.json['faultstring'],
                         'not found')

    @mock.patch.object(metadata, 'err_utils')
    @mock.patch.object(metadata, 'di')
    def test_post_metadata_error(self, mock_di, mock_error_utils):
        mock_error_utils.get_error = self.get_error
        mock_di.resolver.unpack.return_value = get_mocks(error=500)
        response = self.app.post_json(
            '/v1/orm/images/image_id/regions/region_name/metadata',
            metadata_input, expect_errors=True)
        self.assertEqual(500, response.status_code)
        self.assertEqual(response.json['faultstring'],
                         'unknown error')


def get_mocks(error=None):

    metadata_logic = mock.MagicMock()
    utils = mock.MagicMock()
    metadata_logic.add_metadata.return_value = mock.MagicMock()
    if error:
        metadata_logic.add_metadata.side_effect = {404: NotFoundError('not found'),
                                                   500: Exception("unknown error")}[error]
    return metadata_logic, utils
