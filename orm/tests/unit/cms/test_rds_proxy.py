import mock

from orm.common.orm_common.utils.error_base import ErrorStatus
from orm.services.customer_manager.cms_rest.data.sql_alchemy import models
from orm.services.customer_manager.cms_rest import rds_proxy
from orm.tests.unit.cms import FunctionalTest


class Response:
    def __init__(self, status_code, content):
        self.status_code = status_code
        self.content = content

    def json(self):
        return self.content


class TestUtil(FunctionalTest):
    def setUp(self):
        FunctionalTest.setUp(self)
        self.rp = rds_proxy.RdsProxy()

    @mock.patch.object(rds_proxy, 'request')
    @mock.patch('requests.post')
    def testsend_good(self, mock_post, mock_request):
        resp = Response(200, 'my content')
        mock_post.return_value = resp
        send_res = self.rp.send_customer(models.Customer(), "1234", "POST")
        self.assertEqual(send_res, 'my content')

    @mock.patch.object(rds_proxy, 'request')
    @mock.patch('requests.post')
    def test_bad_status(self, mock_post, mock_request):
        resp = Response(400, 'my content')
        mock_post.return_value = resp
        self.assertRaises(ErrorStatus, self.rp.send_customer, models.Customer(), "1234", "POST")

    @mock.patch.object(rds_proxy, 'request')
    @mock.patch('requests.post')
    def test_no_content(self, mock_post, mock_request):
        resp = Response(200, None)
        mock_post.return_value = resp
        self.assertRaises(ErrorStatus, self.rp.send_customer, models.Customer(), "1234", "POST")
