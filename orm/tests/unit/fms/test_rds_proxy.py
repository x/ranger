from orm.services.flavor_manager.fms_rest.data.sql_alchemy import db_models
from orm.services.flavor_manager.fms_rest.proxies import rds_proxy
from orm.tests.unit.fms import FunctionalTest

import mock


class Response:
    def __init__(self, status_code, content):
        self.status_code = status_code
        self.content = content

    def json(self):
        return self.content


class TestUtil(FunctionalTest):

    @mock.patch('orm.services.flavor_manager.fms_rest.proxies.rds_proxy.LOG')
    @mock.patch.object(rds_proxy, 'request')
    @mock.patch('requests.post')
    def test_send_good(self, mock_post, mock_request, mock_log):
        resp = Response(200, 'my content')
        mock_post.return_value = resp
        send_res = rds_proxy.send_flavor(db_models.Flavor().todict(), "1234", "post")
        # self.assertRegexpMatches(mock_log.records[-2].getMessage(), 'Wrapper JSON before sending action')
        # self.assertRegexpMatches(mock_log.records[-1].getMessage(), 'return from rds server status code')

    @mock.patch('orm.services.flavor_manager.fms_rest.proxies.rds_proxy.LOG')
    @mock.patch('requests.post')
    def test_bad_status(self, mock_post, mock_log):
        resp = Response(400, 'my content')
        mock_post.return_value = resp
        # self.assertRegexpMatches(mock_log.records[-2].getMessage(), 'Wrapper JSON before sending action')
        # self.assertRegexpMatches(mock_log.records[-1].getMessage(), 'return from rds server status code')

    @mock.patch('orm.services.flavor_manager.fms_rest.proxies.rds_proxy.LOG')
    @mock.patch('requests.post')
    def test_no_content(self, mock_post, mock_log):
        resp = Response(200, None)
        mock_post.return_value = resp
        # self.assertRaises(ErrorStatus, proxies.rds_proxy.send_flavor, db_models.Flavor(), "1234")
        for r in mock_log.records:
            self.assertNotRegexpMatches(r.getMessage(), 'return from rds server status code')
