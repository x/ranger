import mock
from orm.orm_client.ormcli import cli_common
from unittest import TestCase


class CmsTests(TestCase):

    def respond(self, value, code, headers={}):
        response = mock.Mock()
        response.json.return_value = value
        response.status_code = code
        response.headers = headers
        return response

    @mock.patch.object(cli_common.requests, 'post')
    def test_get_token_errors(self, mock_post):
        # Bad status code
        my_response = mock.MagicMock()
        my_response.status_code = 200
        mock_post.return_value = my_response
        self.assertRaises(cli_common.ConnectionError, cli_common.get_token,
                          3, mock.MagicMock())

        # Post fails
        mock_post.side_effect = ValueError('test')
        self.assertRaises(cli_common.ConnectionError, cli_common.get_token,
                          3, mock.MagicMock(),)

    @mock.patch.object(cli_common.requests, 'post')
    def test_get_token_success(self, mock_post):

        args = mock.MagicMock()

        args.tenant_name = 'tenant_name'
        args.username = 'username'
        args.password = 'password'
        args.auth_region = 'auth_region'
        args.keystone_auth_url = 'url'
        # Bad status code
        my_response = mock.MagicMock()
        my_response.status_code = 201
        mock_post.return_value = self.respond(
            {"access": {"token": {"id": 989}}}, 201, {"x-subject-token": 989})
        self.assertTrue(989, cli_common.get_token(3, args))
