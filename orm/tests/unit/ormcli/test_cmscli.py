from io import StringIO
import json
import mock
import requests
import sys
from unittest import TestCase

from orm.orm_client.ormcli import cmscli
from orm.orm_client.ormcli import ormcli

TJ = {'access': {'token': {'id': 'test'}}}


class CmsTests(TestCase):
    def setUp(self):
        out, sys.stdout, err, sys.stderr = sys.stdout, StringIO(), \
            sys.stderr, StringIO()
        self.mock_response = mock.Mock()

    def respond(self, value, code, headers={}, oy=False):
        # Set the response according to the parameter
        if oy:
            response = mock.Mock()
        else:
            response = self.mock_response

        response.json.return_value = value
        response.status_code = code
        response.headers = headers
        return response

    def test_cmd_details(self):
        # Set up the args parameter
        args = mock.MagicMock()
        args.custid = 'test_custid'
        args.regionid = 'test_region'
        args.userid = 'test_userid'
        args.userdomain = 'test_userdomain'
        args.region = 'test_region'
        args.user = 'test_user'
        args.starts_with = 'test_startswith'
        args.contains = 'test_contains'
        args.force_delete is False

        subcmd_to_result = {
            'create_customer': (requests.post, 'customers/',),
            'delete_customer': (
                requests.delete, 'customers/%s' % args.custid,),
            'update_customer': (requests.put, 'customers/%s' % args.custid,),
            'add_region': (
                requests.post, 'customers/%s/regions' % args.custid,),
            'replace_region': (
                requests.put, 'customers/%s/regions' % args.custid,),
            'delete_region': (
                requests.delete,
                'customers/%s/regions/%s/%s' % (args.custid, args.regionid,
                                                args.force_delete),),
            'add_user': (
                requests.post, 'customers/%s/regions/%s/users' % (
                    args.custid, args.regionid),),
            'replace_user': (
                requests.put,
                'customers/%s/regions/%s/users' % (
                    args.custid, args.regionid),),
            'delete_user': (
                requests.delete, 'customers/%s/regions/%s/users/%s' % (
                    args.custid, args.regionid, args.userid),),
            'add_default_user': (
                requests.post, 'customers/%s/users' % args.custid,),
            'replace_default_user': (
                requests.put, 'customers/%s/users' % args.custid,),
            'delete_default_user': (
                requests.delete, 'customers/%s/users/%s' % (
                    args.custid, args.userid),),
            'add_metadata': (
                requests.post, 'customers/%s/metadata' % args.custid,),
            'replace_metadata': (
                requests.put, 'customers/%s/metadata' % args.custid,),
            'get_customer': (requests.get, 'customers/%s' % args.custid,),
            'list_customers': (requests.get,
                               'customers/?region=%s&user=%s&starts_with=%s'
                               '&contains=%s' % (args.region,
                                                 args.user, args.starts_with,
                                                 args.contains))
        }

        # Assert that each subcommand returns the expected details
        for subcmd in subcmd_to_result:
            args.subcmd = subcmd
            self.assertEqual(subcmd_to_result[subcmd],
                             cmscli.cmd_details(args))

    @mock.patch.object(cmscli, 'validate_args')
    @mock.patch.object(cmscli.requests, 'post')
    @mock.patch.object(cmscli.requests, 'get')
    @mock.patch.object(cmscli.cli_common, 'get_token')
    @mock.patch.object(cmscli, 'globals')
    def test_list_customers(self, mock_globals, mock_get_token,
                            mock_get, mock_post,
                            mock_validate_args):
        mock_post.return_value = self.respond(TJ, 201)
        mock_get.return_value = self.mock_response
        args = ormcli.main('orm cms list_customers t'.split())
        sys.stdout.seek(0)
        output = sys.stdout.read()
        self.assertIn(json.dumps(TJ), output)

    @mock.patch.object(cmscli.requests, 'post')
    @mock.patch.object(cmscli.requests, 'get')
    def test_list_customers_e(self, mock_get, mock_post):
        mock_post.return_value = self.respond(TJ, 200)
        mock_get.side_effect = Exception('e')
        with self.assertRaises(SystemExit) as cm:
            args = ormcli.main('orm cms list_customers t'.split())
        self.assertEqual(cm.exception.code, 1)

    @mock.patch.object(cmscli, 'validate_args')
    @mock.patch.object(cmscli.requests, 'post')
    @mock.patch.object(cmscli.requests, 'get')
    @mock.patch.object(cmscli.cli_common, 'get_token')
    @mock.patch.object(cmscli, 'globals')
    def test_list_customers_errors(self, mock_globals, mock_get_token,
                                   mock_get, mock_post,
                                   mock_validate_args):
        mock_post.return_value = self.respond(TJ, 200)
        mock_get.return_value = self.respond(TJ, 204, oy=True)
        with self.assertRaises(SystemExit) as cm:
            args = ormcli.main('orm cms list_customers t'.split())
        self.assertEqual(cm.exception.code, 0)
        sys.stdout.seek(0)
        output = sys.stdout.read()
        self.assertEqual('', output)

    @mock.patch('requests.get')
    @mock.patch('requests.post')
    def test_list_customers_with_filters(self, mock_post, mock_get):
        cli = ormcli.Cli()
        cli.create_parser()
        cli.parse(
            'orm cms list_customers --region 2 client1'.split())
        resp = self.respond('{"Hi, mom"}', 200, {'X-Subject-Token': 989})
        mock_post.return_value = self.respond(
            {"access": {"token": {"id": 989}}}, 200)
