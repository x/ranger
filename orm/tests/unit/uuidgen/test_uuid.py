from orm.common.orm_common.utils.error_base import NotFoundError, UnauthorizedError
from orm.services.id_generator.uuidgen.controllers.v1 import uuid_controller
from orm.tests.unit.uuidgen import FunctionalTest

from mock import MagicMock, patch

mock_data_manager = None
flow_type = 0


class TestUuidController(FunctionalTest):
    def setUp(self):
        FunctionalTest.setUp(self)

        uuid_controller.authentication = MagicMock()
        uuid_controller.DBManager = get_mock_datamanager

        global flow_type
        flow_type = 0

    def tearDown(self):
        FunctionalTest.tearDown(self)

    @patch.object(uuid_controller, 'utils')
    @patch.object(uuid_controller, 'DBManager')
    @patch.object(uuid_controller, 'request')
    def test_delete_uuid_success(self, mock_request,
                                 mock_dbmanager, mock_utils):

        mock_request.headers.get.return_value = 'value'
        mock_dbmanager.delete_uuid.return_value = 1
        mock_utils.get_region_end_point_from_db.return_value = 'ep'

        response = self.app.delete('/v1/uuids/test_uuid')
        self.assertEqual(response.status_code, 200)

    @patch.object(uuid_controller, 'utils')
    @patch.object(uuid_controller, 'DBManager')
    @patch.object(uuid_controller, 'request')
    def test_delete_uuid_missing_parameter(self, mock_request,
                                           mock_dbmanager, mock_utils):

        mock_request.headers.get.return_value = None
        mock_utils.get_region_end_point_from_db.return_value = 'ep'

        response = self.app.delete('/v1/uuids/test_uuid', expect_errors=True)
        self.assertEqual(response.status_code, 400)

    @patch.object(uuid_controller, 'utils')
    @patch.object(uuid_controller, 'DBManager')
    @patch.object(uuid_controller, 'request')
    def test_delete_uuid_endpoint_not_found(self, mock_request,
                                            mock_dbmanager, mock_utils):

        mock_request.headers.get.return_value = 'value'
        mock_utils.get_region_end_point_from_db.side_effect = NotFoundError()

        response = self.app.delete('/v1/uuids/test_uuid', expect_errors=True)
        self.assertEqual(response.status_code, 404)

    @patch.object(uuid_controller, 'utils')
    @patch.object(uuid_controller, 'DBManager')
    @patch.object(uuid_controller, 'request')
    def test_delete_uuid_failed_authentication(self, mock_request,
                                               mock_dbmanager, mock_utils):

        mock_request.headers.get.return_value = 'value'
        mock_utils.get_region_end_point_from_db.return_value = 'ep'
        uuid_controller.authentication.authorize.side_effect = UnauthorizedError(status_code=401)

        response = self.app.delete('/v1/uuids/test_uuid', expect_errors=True)
        self.assertEqual(response.status_code, 401)

    @patch.object(uuid_controller, 'utils')
    @patch.object(uuid_controller, 'request')
    def test_delete_uuid_not_found(self, mock_request, mock_utils):
        global flow_type
        flow_type = 1

        mock_request.headers.get.return_value = 'value'
        mock_utils.get_region_end_point_from_db.return_value = 'ep'

        response = self.app.delete('/v1/uuids/test_uuid', expect_errors=True)
        self.assertEqual(response.status_code, 404)

    @patch.object(uuid_controller, 'utils')
    @patch.object(uuid_controller, 'request')
    def test_delete_uuid_database_error(self, mock_request, mock_utils):
        global flow_type
        flow_type = 2

        mock_request.headers.get.return_value = 'value'
        mock_utils.get_region_end_point_from_db.return_value = 'ep'

        response = self.app.delete('/v1/uuids/test_uuid', expect_errors=True)
        self.assertEqual(response.status_code, 500)


def get_mock_datamanager():
    global data_manager_mock
    data_manager_mock = MagicMock()

    if flow_type == 1:
        data_manager_mock.delete_uuid.return_value = 0
    elif flow_type == 2:
        data_manager_mock.delete_uuid.side_effect = SystemError()

    return data_manager_mock
