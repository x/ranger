import json
import logging
from unittest import mock, TestCase

import mock
from orm.common.orm_common.hooks import api_error_hook

logger = logging.getLogger(__name__)


class TestAPIErrorHook(TestCase):

    @mock.patch.object(api_error_hook, 'err_utils')
    @mock.patch.object(api_error_hook, 'json')
    def test_after_401(self, mock_json, mock_err_utils):
        a = api_error_hook.APIErrorHook()
        state = mock.MagicMock()

        mock_err_utils.get_error_dict.return_value = 'B'
        mock_json.loads = json.loads
        mock_json.dumps = json.dumps
        state.response.status_code = 401
        a.after(state)
        self.assertEqual(state.response.body,
                         json.dumps(mock_err_utils.get_error_dict.return_value).encode("UTF-8"))

    @mock.patch.object(api_error_hook, 'err_utils')
    def test_after_not_an_error(self, mock_err_utils):
        a = api_error_hook.APIErrorHook()
        state = mock.MagicMock()

        mock_err_utils.get_error_dict.return_value = 'B'
        state.response.body = 'AAAA'
        temp = state.response.json
        # A successful status code
        state.response.status_code = 201
        a.after(state)
        # Assert that the response body hasn't changed
        self.assertEqual(state.response.json, temp)

    @mock.patch.object(api_error_hook, 'err_utils')
    @mock.patch.object(api_error_hook, 'json')
    def test_after_error(self, mock_json, mock_err_utils):
        # real object to test mock state
        a = api_error_hook.APIErrorHook()
        # create mock state
        state = mock.MagicMock()
        # assign relevant state values for testing failure of api_error_hook
        state.response.status_code = 402
        state.response.body = json.dumps({'AAAA': 'a'}).encode("UTF-8")

        # re-create json loading functionality but produces mock side effect
        mock_json.dumps = json.dumps
        mock_json.loads = json.loads
        mock_json.loads = mock.Mock(side_effect=ValueError('sd'))

        # assume err_utils will return this content and will set state.response.body to it
        mock_err_utils.get_error_dict.return_value = json.loads('{"B" : "b"}')
        a.after(state)

        # assert previous assumption is true
        self.assertEqual(json.loads(state.response.body),
                         mock_err_utils.get_error_dict.return_value)

    @mock.patch.object(api_error_hook, 'err_utils')
    @mock.patch.object(api_error_hook, 'json')
    def test_after_success(self, mock_json, mock_err_utils):
        a = api_error_hook.APIErrorHook()
        state = mock.MagicMock()
        state.response.status_code = 404
        state.response.body = json.dumps({"debuginfo": "null", "faultcode": "Client", "faultstring": '{"type": "Not Found", "created": "1475768730.95", "message": "customer: q not found"}'}).encode("UTF-8")
        mock_json.loads = json.loads
        mock_json.dumps = json.dumps
        mock_err_utils.get_error_dict.return_value = json.loads('{"message": "customer: q not found", "created": "1475768730.95", "type": "Not Found", "details": "", "code": 404, "transaction_id": "mock_json5efa7416fb4d408cc0e30e4373cf00"}')
        a.after(state)
        self.assertEqual(json.loads(state.response.body), mock_err_utils.get_error_dict.return_value)
