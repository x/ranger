from . import config
import json
import requests

OK_CODE = 200

ORM_CLIENT_KWARGS = {'type': str, 'help': 'client name', 'default': None,
                     'nargs': '?'}


class MissingArgumentError(Exception):
    """Should be raised when an argument was found missing by CLI logic."""
    pass


class ConnectionError(Exception):
    pass


class ResponseError(Exception):
    pass


def get_token(timeout, args):
    headers = {
        'Content-Type': 'application/json',
    }
    url = '%s/v3/auth/tokens'
    data = '''
{
   "auth":{
      "identity":{
         "methods":[
            "password"
         ],
         "password":{
            "user":{
               "domain":{
                  "name":"%s"
               },
               "name":"%s",
               "password":"%s"
            }
         }
      },
      "scope":{
         "project":{
            "name":"%s",
            "domain":{
               "id":"%s"
            }
         }
      }
   }
}'''
    for argument in ('tenant_name', 'username', 'password', 'auth_region', 'keystone_auth_url'):
        argument_value = getattr(args, argument, None)
        if argument_value is not None:
            globals()[argument] = argument_value
        else:
            configuration_value = getattr(config, argument)
            if configuration_value:
                globals()[argument] = configuration_value
            else:
                message = ('ERROR: {} for token generation was not supplied. '
                           'Please use its command-line argument or '
                           'environment variable.'.format(argument))
                print(message)
                raise MissingArgumentError(message)

    keystone_ep = args.keystone_auth_url if args.keystone_auth_url else None
    if keystone_ep is None:
        raise ConnectionError(
            'Failed in get_token, keystone endpoint not define')

    user_domain = args.user_domain if args.user_domain else 'default'
    project_domain = args.project_domain if args.project_domain else 'default'
    url = url % (keystone_ep,)
    data = data % (user_domain,
                   username,
                   password,
                   tenant_name,
                   project_domain,)

    if args.verbose:
        print((
            "Getting token:\ntimeout: %d\nheaders: %s\nurl: %s\n" % (
                timeout, headers, url)))
    try:
        resp = requests.post(url, timeout=timeout, data=data, headers=headers)
        if resp.status_code != 201:
            raise ResponseError(
                'Failed to get token (Reason: {})'.format(
                    resp.status_code))
        return resp.headers['x-subject-token']

    except Exception as e:
        print(str(e))
        raise ConnectionError(str(e))


def pretty_print_json(json_to_print):
    """Print a json without the u' prefix."""
    print((json.dumps(json_to_print)))
